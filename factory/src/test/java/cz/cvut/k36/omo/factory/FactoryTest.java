package cz.cvut.k36.omo.factory;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FactoryTest {

    //arrange
    private ByteArrayOutputStream outContent;
    private final PrintStream originalOut = System.out;

    private void newOutputForTest(){
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    @ParameterizedTest(name = "Priority – {0}, count – {1}, type - {2}")
    @CsvSource({"LOW, 10, CAR", "NORMAL, 5, BUS", "LOW, 1, CARAVAN"})
    public void factory_inputContractsAndWaitingForTheirCompletion_contractCompletionControl(String priority, int count, String type){
        newOutputForTest();
        //arrange
        int controlPosition = -1;
        String command = "add contract low car 1000 10" + System.lineSeparator()
                + "add contract normal bus 4657 5" + System.lineSeparator()
                + "n 20" + System.lineSeparator()
                + "2" + System.lineSeparator()
                + "2" + System.lineSeparator()
                + "add money 1000" + System.lineSeparator()
                + "add contract low caravan 1465 1" + System.lineSeparator()
                + "repair info 10" + System.lineSeparator()
                + "n 100" + System.lineSeparator()
                + "quit" + System.lineSeparator()
                + "yes" + System.lineSeparator();
        ByteArrayInputStream inContent = new ByteArrayInputStream(command.getBytes());
        System.setIn(inContent);

        //action
        StartSimulation.main(null);
        controlPosition = outContent.toString().indexOf("Contract of priority "+ priority +" for "+ count+" (done "+count+") "+type+"s finished");

        //assert
        assertTrue(controlPosition > 0);
        System.setOut(originalOut);
    }

    @ParameterizedTest(name = "First contract: Priority – {0}, count – {1}, type - {2}. Second contract: Priority – {3}, count – {4}, type - {5}." )
    @CsvSource({"NORMAL, 5, BUS, LOW, 10, CAR", "LOW, 10, CAR, LOW, 1, CARAVAN"})
    public void factory_inputContractsAndWaitingForTheirCompletion_validationOfCompletionOrder(String priority1, int count1, String type1, String priority2, int count2, String type2){
        newOutputForTest();

        //arrange
        int position1 = -1;
        int position2 = -1;
        String command = "add contract low car 1000 10" + System.lineSeparator()
                + "add contract normal bus 4657 5" + System.lineSeparator()
                + "n 20" + System.lineSeparator()
                + "2" + System.lineSeparator()
                + "2" + System.lineSeparator()
                + "add money 1000" + System.lineSeparator()
                + "add contract low caravan 1465 1" + System.lineSeparator()
                + "repair info 10" + System.lineSeparator()
                + "n 100" + System.lineSeparator()
                + "quit" + System.lineSeparator()
                + "yes" + System.lineSeparator();
        ByteArrayInputStream inContent = new ByteArrayInputStream(command.getBytes());
        System.setIn(inContent);

        //action
        StartSimulation.main(null);
        position1 = outContent.toString().indexOf("Contract of priority "+ priority1 +" for "+ count1+" (done "+count1+") "+type1+"s finished");
        position2 = outContent.toString().indexOf("Contract of priority "+ priority2 +" for "+ count2+" (done "+count2+") "+type2+"s finished");

        //assert
        assertTrue(position1 < position2);
        System.setOut(originalOut);
    }


    @ParameterizedTest(name = "visitDirectorControl")
    @CsvSource({"LOW, 10, CAR"})
    public void factory_inputContractsAndWaitingForTheirCompletion_visitDirectorControl(String priority, int count, String type){
        newOutputForTest();

        //arrange
        String command = "add contract low car 1000 10" + System.lineSeparator()
                + "add contract normal bus 4657 5" + System.lineSeparator()
                + "n 20" + System.lineSeparator()
                + "2" + System.lineSeparator()
                + "2" + System.lineSeparator()
                + "add money 1000" + System.lineSeparator()
                + "add contract low caravan 1465 1" + System.lineSeparator()
                + "visit director" + System.lineSeparator()
                + "quit" + System.lineSeparator()
                + "yes" + System.lineSeparator();
        String res = "\tConveyor for product BUS with current contract \"null\"" + System.lineSeparator()
                + "\tConveyor for product CAR with current contract \"null\"";
        ByteArrayInputStream inContent = new ByteArrayInputStream(command.getBytes());
        System.setIn(inContent);

        //action
        StartSimulation.main(null);

        //assert
        assertTrue(outContent.toString().indexOf(res) > 0);
        System.setOut(originalOut);
    }

    @ParameterizedTest(name = "Priority – {0}, price – {1}, type - {2}, tact - {3}")
    @CsvSource({"LOW, 1000, CAR, 8", "NORMAL, 4657, BUS, 14", "HIGH, 5000, MOTORBIKE, 5"})
    public void factory_inputOfOneContractWithLimitedExpectation_successfulCompletionOfTheContract(String priority, int price, String type, int takt){
        newOutputForTest();

        //arrange
        String command = "add contract " + priority + " " + type + " " + price + " 1" + System.lineSeparator()
                + "n " + takt + System.lineSeparator()
                + "2" + takt + System.lineSeparator()
                + "2" + System.lineSeparator()
                + "quit" + System.lineSeparator()
                + "yes" + System.lineSeparator();
        ByteArrayInputStream inContent = new ByteArrayInputStream(command.getBytes());
        System.setIn(inContent);

        //action
        StartSimulation.main(null);

        //assert
        assertTrue(outContent.toString().indexOf("Contract of priority "+ priority +" for "+ 1+" (done "+1+") "+type+"s finished") > 0);
        System.setOut(originalOut);
    }
}
