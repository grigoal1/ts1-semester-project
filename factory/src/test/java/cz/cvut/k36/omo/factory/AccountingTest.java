package cz.cvut.k36.omo.factory;


import cz.cvut.k36.omo.factory.worker.Accounting;
import cz.cvut.k36.omo.factory.worker.RepairMan;
import cz.cvut.k36.omo.factory.worker.Worker;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AccountingTest {

    @ParameterizedTest(name = "Worker wage - {0}, hours to billing – {1}, result wage – {2}")
    @CsvSource({"100, 10, 1000", "150, 10, 1500"})
    public void accounting_controlOfTheWagesOfOneWorker_wages(int workerWages, int hoursToBilling, int expWage){
        //arrange
        int wage = 0;
        Accounting accounting = new Accounting(workerWages, 100);
        Worker worker = mock(Worker.class);
        when(worker.getHoursToBilling()).thenReturn(hoursToBilling);
        ArrayList<Worker> workers= new ArrayList<>();

        //action
        workers.add(worker);
        wage = accounting.payWorkerWages(workers);

        //assert
        assertEquals(expWage, wage);
    }

    /**
     *  Během testování byla nalezena chyba, která se objeví v tomto testu. To spočívá ve skutečnosti,
     *  že Accounting nesprávně spočitá platu RepairMan. V podrobnější studii třídy bylo zjištěno,
     *  že má soukromou funkci, která každému přiřazuje plat, který patří Warker.
     */
    @ParameterizedTest(name = "RepairMan wage - {0}, hours to billing – {1}, result wage – {2}")
    @CsvSource({"100, 10, 1000", "150, 10, 1500"})
    public void accounting_controlOfTheWagesOfOneRepairMan_wages(int repairManWages, int hoursToBilling, int expWage){
        //arrange
        int wage = 0;
        Accounting accounting = new Accounting(100, repairManWages);
        RepairMan repairMan = mock(RepairMan.class);
        when(repairMan.getHoursToBilling()).thenReturn(hoursToBilling);
        ArrayList<RepairMan> repairMen= new ArrayList<>();

        //action
        repairMen.add(repairMan);
        wage = accounting.payRepairManWages(repairMen);

        //assert
        assertEquals(expWage, wage);
    }

    @ParameterizedTest(name = "Workers wage - {0}, hours to billing first worker – {1},, hours to billing second worker – {2}, result wage – {3}")
    @CsvSource({"100, 10, 20, 3000", "150, 10, 15, 3750"})
    public void accounting_controlOfTheWagesOfTwoWorkers_sumOfWages(int workerWages, int hoursToBilling1, int hoursToBilling2, int expWage){
        //arrange
        int wage = 0;
        Accounting accounting = new Accounting(workerWages, 100);
        Worker worker1 = mock(Worker.class);
        Worker worker2 = mock(Worker.class);
        when(worker1.getHoursToBilling()).thenReturn(hoursToBilling1);
        when(worker2.getHoursToBilling()).thenReturn(hoursToBilling2);
        ArrayList<Worker> workers= new ArrayList<>();

        //action
        workers.add(worker1);
        workers.add(worker2);
        wage = accounting.payWorkerWages(workers);

        //verification that each is assigned his hours to billing
        verify(worker1, times(1)).getHoursToBilling();
        verify(worker2, times(1)).getHoursToBilling();

        //assert
        assertEquals(expWage, wage);
    }


    /**
     *  Během testování byla nalezena chyba, která se objeví v tomto testu. To spočívá ve skutečnosti,
     *  že Accounting nesprávně spočitá platu RepairMan. V podrobnější studii třídy bylo zjištěno,
     *  že má soukromou funkci, která každému přiřazuje plat, který patří Warker.
     */
    @ParameterizedTest(name = "RepairMan wage - {0}, hours to billing first repairman – {1},, hours to billing second repairman – {2}, result wage – {3}")
    @CsvSource({"100, 10, 20, 3000", "150, 10, 15, 3750"})
    public void accounting_controlOfTheWagesOfTwoRepairMen_sumOfWages(int repairManWages, int hoursToBilling1, int hoursToBilling2, int expWage){
        //arrange
        int wage = 0;
        Accounting accounting = new Accounting(100, repairManWages);
        RepairMan repairMan1 = mock(RepairMan.class);
        RepairMan repairMan2 = mock(RepairMan.class);
        when(repairMan1.getHoursToBilling()).thenReturn(hoursToBilling1);
        when(repairMan2.getHoursToBilling()).thenReturn(hoursToBilling2);
        ArrayList<RepairMan> repairMen= new ArrayList<>();


        //action
        repairMen.add(repairMan1);
        repairMen.add(repairMan2);
        wage = accounting.payRepairManWages(repairMen);

        ////verification that each is assigned his hours to billing
        verify(repairMan1, times(1)).getHoursToBilling();
        verify(repairMan2, times(1)).getHoursToBilling();
        
        //assert
        assertEquals(expWage, wage);
    }
}
