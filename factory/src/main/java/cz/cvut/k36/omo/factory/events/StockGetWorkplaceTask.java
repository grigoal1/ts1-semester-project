
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.supply.Stock;
import cz.cvut.k36.omo.factory.production.WorkplaceType;

/**
 * Task for a Stock to get a Workplace. Received Workplace can be specified by an ID, or
 * it can be asked for any free Workplace or it can be asked for all Workplaces in
 * the Stock.
 */
public class StockGetWorkplaceTask extends Task {

    public enum WorkplaceIdentification {
        ID,
        FREE,
        ALL;
    }
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final WorkplaceIdentification workplaceIdentification;
    private final WorkplaceType workplaceType;
    private final int amount;
    private final int workplaceID;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    /**
     * Creates a Task to get amount of FREE Workplaces from the Stock.
     * 
     * @param sender task's sender
     * @param priority task's priority
     * @param type type of the workplace
     * @param amount amount of wanted workplaces
     */
    public StockGetWorkplaceTask(EventChannelAccessible sender, Priority priority, WorkplaceType type, int amount) {
        super(priority, sender, Stock.class);
        this.workplaceType = type;
        this.workplaceID = -1;
        this.amount = amount;
        this.workplaceIdentification = WorkplaceIdentification.FREE;
    }
    
    /**
     * Creates a Task to get a Workplace with the given ID from the Stock.
     * 
     * @param sender task's sender
     * @param priority task's priority
     * @param ID ID of the wanted workplace
     */
    public StockGetWorkplaceTask(EventChannelAccessible sender, Priority priority, int ID) {
        super(priority, sender, Stock.class);
        this.workplaceType = null;
        this.workplaceID = ID;
        this.amount = -1;
        this.workplaceIdentification = WorkplaceIdentification.ID;
    }
    
    /**
     * Creates a Task to get all Workplaces in the Stock.
     * 
     * @param sender task's sender
     * @param priority task's priority
     */
    public StockGetWorkplaceTask(EventChannelAccessible sender, Priority priority) {
        super(priority, sender, Stock.class);
        this.workplaceType = null;
        this.workplaceID = -1;
        this.amount = -1;
        this.workplaceIdentification = WorkplaceIdentification.ALL;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * @return workplaceType or null if getWorkplaceIdentification() != FREE
     */
    public WorkplaceType getWorkplaceType() {
        return workplaceType;
    }

    /**
     * @return workplaceID or -1 if getWorkplaceIdentification() != ID
     */
    public int getWorkplaceID() {
        return workplaceID;
    }

    /**
     * @return amount of asked Workplaces or -1 if getWorkplaceIdentification() != FREE
     */
    public int getAmount() {
        return amount;
    }

    public WorkplaceIdentification getWorkplaceIdentification() {
        return workplaceIdentification;
    }
    
    @Override
    public String toString() {
        switch (workplaceIdentification) {
            case ID:
                return this.getClass().getSimpleName() + " from " + sender + " to get Workplace with ID " + workplaceID;
            case FREE:
                return this.getClass().getSimpleName() + " from " + sender + " to a free Workplace";
            case ALL:
                return this.getClass().getSimpleName() + " from " + sender + " to get all Workplaces";
        }
        return this.getClass().getSimpleName();
    }
}

