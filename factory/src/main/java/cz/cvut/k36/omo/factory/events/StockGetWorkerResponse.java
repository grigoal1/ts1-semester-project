
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.worker.Worker;
import java.util.List;

/**
 * Response to a StockGetWorkerTask containing a list of received Workers from a Stock.
 */
public class StockGetWorkerResponse extends Response {

    private final List<Worker> workers;
    
    public StockGetWorkerResponse(EventChannelAccessible sender, StockGetWorkerTask previousTask, List<Worker> workers) {
        super(sender, previousTask);
        this.workers = workers;
    }

    public List<Worker> getWorkers() {
        return workers;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " returning Workers: " + workers + " from " + sender + " to " + previousTask.sender;
    }
}
