
package cz.cvut.k36.omo.factory.events;

/**
 * Response to MoneyNeededTask containing money given by a user.
 */
public class MoneyNeededResponse extends Response {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int amount;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public MoneyNeededResponse(EventChannelAccessible sender, MoneyNeededTask previousTask, int amount) {
        super(sender, previousTask);
        this.amount = amount;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public int getAmount() {
        return amount;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " returning given " + amount + " of money";
    }
}
