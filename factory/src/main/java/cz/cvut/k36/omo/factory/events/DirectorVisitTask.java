
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.control.Director;

/**
 * Task for a  Director to visit the Factory.
 */
public class DirectorVisitTask extends Task {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public DirectorVisitTask(EventChannelAccessible sender) {
        super(Priority.HIGH, sender, Director.class);
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
}
