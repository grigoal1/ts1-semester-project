
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.production.Production;
import cz.cvut.k36.omo.factory.worker.RepairMan;

/**
 * Task for a Production to add a new RepairMan.
 */
public class ProductionAddRepairManTask extends Task {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final RepairMan repairMan;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public ProductionAddRepairManTask(EventChannelAccessible sender, Priority priority, RepairMan repairMan) {
        super(priority, sender, Production.class);
        this.repairMan = repairMan;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public RepairMan getRepairMan() {
        return repairMan;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " from " + sender + " to add RepairMan " + repairMan;
    }
}
