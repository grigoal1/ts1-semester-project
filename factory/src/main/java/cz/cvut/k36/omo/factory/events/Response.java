
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;

/**
 * Response for a task.
 */
public abstract class Response {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    protected final EventChannelAccessible sender;
    protected final Task previousTask;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    protected Response(EventChannelAccessible sender, Task previousTask) {
        this.sender = sender;
        this.previousTask = previousTask;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public Priority getPriority() {
        return previousTask.getPriority();
    }
    
    public EventChannelAccessible getSender() {
        return sender;
    }
    
    public EventChannelAccessible getRecipient() {
        return previousTask.getSender();
    }
    
    public Task getTask() {
        return previousTask;
    }
}
