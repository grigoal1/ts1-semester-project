
package cz.cvut.k36.omo.factory.production;

/**
 * Robot processing a Product. Needs to be placed in a WorkplaceHolder for
 * a Product to be passed by chain of responsibility.
 */
public class Robot extends Workplace {
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Robot(RobotType robotType) {
        super(robotType);
    }
    
    /* ----------------------- PACKAGE METHODS ----------------------- */
    
    /**
     * @return False if the Robot is broken, true if not and is ready to process
     */
    @Override
    boolean isReady() {
        return broken == 0;
    }
    
    @Override
    boolean isMachine() {
        return false;
    }
    
    @Override
    boolean isRobot() {
        return true;
    }

    /* ----------------------- PROTECTED METHODS ----------------------- */
    
    @Override
    protected void specialProcessFeature() {
        // No special feature of Robot
    }

    @Override
    protected void specialBreakDownFeature() {
        // No special feature of Robot
    }
}
