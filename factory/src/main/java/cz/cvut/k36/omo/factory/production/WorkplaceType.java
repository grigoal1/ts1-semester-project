
package cz.cvut.k36.omo.factory.production;

/**
 * Type of Workplace and its general characteristics.
 */
public interface WorkplaceType {
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    /**
     * @return Basic price used in Store (the real price is derived from this
     * value)
     */
    public int getBasePrice();
    /**
     * @return Consumption of electricity in one tact
     */
    public int getElectricityConsumption();
    /**
     * @return Consumption of oil in one tact
     */
    public int getOilConsumption();
    /**
     * @return Basic number of tacts passed before first Workplace crash
     */
    public int getTactsToFault();
    /**
     * @return Basic number of tacts needed for the Workplace to be repaired
     */
    public int getTactsForRepair();
    
    public boolean isMachineType();
    
    public boolean isRobotType();
}
