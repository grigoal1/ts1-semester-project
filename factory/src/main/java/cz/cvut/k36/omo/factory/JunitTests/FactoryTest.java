//package cz.cvut.k36.omo.factory.JunitTests;
//
//import cz.cvut.k36.omo.factory.Factory;
//import cz.cvut.k36.omo.factory.FactoryBuilder;
//import org.junit.jupiter.api.Test;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.PrintStream;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class FactoryTest {
//
//    @Test
//    public void HelpTest() {
//        String rez = "\n" +
//                "Factory waits for command (type \"help\" to show help): Use these commands to control the factory (note that commands are NOT case sensitive):\n" +
//                "\"n [<NUMBER> [postpone]]\", \"next [<NUMBER> [postpone]]\"\n" +
//                "\tNext NUMBER steps of processing contracts in the factory is done, if NUMBER is not set, 1 step is done. NUMBER should be a positive integer. If \"postpone\" is set as third parameter, all contract conflicts will be solved by postponing. This command ends adding commands in the current tact and starts processing in the factory\n" +
//                "\"add contract <PRIORITY> <TYPE> <PRICE> <AMOUNT>\"\n" +
//                "\tA new contract for the factory is declared. PRIORITY should be string LOW/NORMAL/HIGH, TYPE should be CAR/MOTORBIKE/TRUCK/BUS/CARAVAN, PRICE and AMOUNT should be positive integer values\n" +
//                "\"add money <AMOUNT>\"\n" +
//                "\tFactory receives given amount of money. MONEY should be a positive integer value\n" +
//                "\"quit\"\n" +
//                "\tFactory is exited (that means destoyed, all progress is lost)\n" +
//                "\"report <TYPE> [<START> <END>]\"\n" +
//                "\tGiven TYPE of report is created. TYPE should be CONFIGURATION/EVENT/CONSUMPTION/OUTTAGES. If START and END are given, only data from tacts number START to END are processed. START and END should be integer between 1 to current tact number\n" +
//                "\"visit <VISITOR>\"\n" +
//                "\tGiven VISITOR visits the factory. VISITOR should be \"DIRECTOR\" or \"INSPECTOR ID\", where ID is an ID of a conveyor to be inspected\n" +
//                "\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): Type \"yes\" to confirm exiting the factory: ";
//        String command = "help\n" +
//                "quit\n" +
//                "yes\n";
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//
//        PrintStream old = System.out;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
////        System.out.println("Start");
//        Factory f = FactoryBuilder.createDefaultFactory().setMinimalPercentProfit(15).getFactory();
//        f.run();
//        System.setOut(old);
//        System.out.println(baos.toString());
//        if(baos.toString().contentEquals(rez)){
//            assertTrue(baos.toString().contentEquals(rez));
//        }else {
//            assertSame(baos.toString(), rez);
//        }
//    }
//
//    @Test
//    public void CarTest() {
//        String rez = "\n" +
//                "Factory waits for command (type \"help\" to show help): Given contract was added into the Production as pending. Production will try to start it in next tact.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 1: **********\n" +
//                "Factory ended this tact with 150000 money, 1000 electricity, 1000 oil, 10000 steal.\n" +
//                "********** Tact 2: **********\n" +
//                "Factory ended this tact with 150000 money, 997 electricity, 998 oil, 9900 steal.\n" +
//                "********** Tact 3: **********\n" +
//                "Factory ended this tact with 150000 money, 992 electricity, 994 oil, 9800 steal.\n" +
//                "********** Tact 4: **********\n" +
//                "Factory ended this tact with 150000 money, 985 electricity, 990 oil, 9700 steal.\n" +
//                "********** Tact 5: **********\n" +
//                "Factory ended this tact with 150000 money, 975 electricity, 986 oil, 9600 steal.\n" +
//                "********** Tact 6: **********\n" +
//                "Factory ended this tact with 150000 money, 964 electricity, 981 oil, 9500 steal.\n" +
//                "********** Tact 7: **********\n" +
//                "Factory ended this tact with 150000 money, 952 electricity, 975 oil, 9400 steal.\n" +
//                "********** Tact 8: **********\n" +
//                "Product CAR-1 in stage 6 from Contract of priority LOW for 10 (done 0) CARs finished\n" +
//                "Factory ended this tact with 150000 money, 940 electricity, 969 oil, 9300 steal.\n" +
//                "********** Tact 9: **********\n" +
//                "Product CAR-2 in stage 6 from Contract of priority LOW for 10 (done 1) CARs finished\n" +
//                "Factory ended this tact with 150000 money, 928 electricity, 963 oil, 9200 steal.\n" +
//                "********** Tact 10: **********\n" +
//                "Product CAR-3 in stage 6 from Contract of priority LOW for 10 (done 2) CARs finished\n" +
//                "Factory ended this tact with 150000 money, 916 electricity, 957 oil, 9100 steal.\n" +
//                "********** Tact 11: **********\n" +
//                "Product CAR-4 in stage 6 from Contract of priority LOW for 10 (done 3) CARs finished\n" +
//                "Factory ended this tact with 150000 money, 904 electricity, 951 oil, 9000 steal.\n" +
//                "********** Tact 12: **********\n" +
//                "Product CAR-5 in stage 6 from Contract of priority LOW for 10 (done 4) CARs finished\n" +
//                "Factory ended this tact with 150000 money, 895 electricity, 947 oil, 9000 steal.\n" +
//                "********** Tact 13: **********\n" +
//                "Product CAR-6 in stage 6 from Contract of priority LOW for 10 (done 5) CARs finished\n" +
//                "Factory ended this tact with 150000 money, 888 electricity, 945 oil, 9000 steal.\n" +
//                "********** Tact 14: **********\n" +
//                "Product CAR-7 in stage 6 from Contract of priority LOW for 10 (done 6) CARs finished\n" +
//                "Factory ended this tact with 150000 money, 883 electricity, 943 oil, 9000 steal.\n" +
//                "********** Tact 15: **********\n" +
//                "Product CAR-8 in stage 6 from Contract of priority LOW for 10 (done 7) CARs finished\n" +
//                "Factory ended this tact with 150000 money, 881 electricity, 941 oil, 9000 steal.\n" +
//                "********** Tact 16: **********\n" +
//                "Product CAR-9 in stage 6 from Contract of priority LOW for 10 (done 8) CARs finished\n" +
//                "Factory ended this tact with 150000 money, 880 electricity, 940 oil, 9000 steal.\n" +
//                "********** Tact 17: **********\n" +
//                "Product CAR-10 in stage 6 from Contract of priority LOW for 10 (done 9) CARs finished\n" +
//                "Contract of priority LOW for 10 (done 10) CARs finished\n" +
//                "Factory ended this tact with 200000 money, 880 electricity, 940 oil, 9000 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): Type \"yes\" to confirm exiting the factory: ";
//        String command = "add contract low car 10000 10\n" +
//                "next 17\n" +
//                "quit\n" +
//                "yes";
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//
//        PrintStream old = System.out;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
//        Factory f = FactoryBuilder.createDefaultFactory().setMinimalPercentProfit(15).getFactory();
//        f.run();
//        System.setOut(old);
//        System.out.println(baos.toString());
//        if(baos.toString().contentEquals(rez)){
//            assertTrue(baos.toString().contentEquals(rez));
//        }else {
//            assertSame(baos.toString(), rez);
//        }
//    }
//
//    @Test
//    public void MotorbikeTest() {
//        String rez = "\n" +
//                "Factory waits for command (type \"help\" to show help): Given contract was added into the Production as pending. Production will try to start it in next tact.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 18: **********\n" +
//                "Factory ended this tact with 150000 money, 1000 electricity, 1000 oil, 10000 steal.\n" +
//                "********** Tact 19: **********\n" +
//                "Factory ended this tact with 150000 money, 998 electricity, 998 oil, 9980 steal.\n" +
//                "********** Tact 20: **********\n" +
//                "Factory ended this tact with 150000 money, 993 electricity, 996 oil, 9960 steal.\n" +
//                "********** Tact 21: **********\n" +
//                "Factory ended this tact with 150000 money, 987 electricity, 993 oil, 9940 steal.\n" +
//                "********** Tact 22: **********\n" +
//                "Product MOTORBIKE-11 in stage 3 from Contract of priority LOW for 10 (done 0) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 150000 money, 981 electricity, 990 oil, 9920 steal.\n" +
//                "********** Tact 23: **********\n" +
//                "Product MOTORBIKE-12 in stage 3 from Contract of priority LOW for 10 (done 1) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 150000 money, 975 electricity, 987 oil, 9900 steal.\n" +
//                "********** Tact 24: **********\n" +
//                "Product MOTORBIKE-13 in stage 3 from Contract of priority LOW for 10 (done 2) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 150000 money, 969 electricity, 984 oil, 9880 steal.\n" +
//                "********** Tact 25: **********\n" +
//                "Product MOTORBIKE-14 in stage 3 from Contract of priority LOW for 10 (done 3) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 150000 money, 963 electricity, 981 oil, 9860 steal.\n" +
//                "********** Tact 26: **********\n" +
//                "Product MOTORBIKE-15 in stage 3 from Contract of priority LOW for 10 (done 4) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 150000 money, 957 electricity, 978 oil, 9840 steal.\n" +
//                "********** Tact 27: **********\n" +
//                "Product MOTORBIKE-16 in stage 3 from Contract of priority LOW for 10 (done 5) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 150000 money, 951 electricity, 975 oil, 9820 steal.\n" +
//                "********** Tact 28: **********\n" +
//                "Product MOTORBIKE-17 in stage 3 from Contract of priority LOW for 10 (done 6) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 150000 money, 945 electricity, 972 oil, 9800 steal.\n" +
//                "********** Tact 29: **********\n" +
//                "Product MOTORBIKE-18 in stage 3 from Contract of priority LOW for 10 (done 7) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 150000 money, 941 electricity, 971 oil, 9800 steal.\n" +
//                "********** Tact 30: **********\n" +
//                "Product MOTORBIKE-19 in stage 3 from Contract of priority LOW for 10 (done 8) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 150000 money, 940 electricity, 970 oil, 9800 steal.\n" +
//                "********** Tact 31: **********\n" +
//                "Product MOTORBIKE-20 in stage 3 from Contract of priority LOW for 10 (done 9) MOTORBIKEs finished\n" +
//                "Contract of priority LOW for 10 (done 10) MOTORBIKEs finished\n" +
//                "Factory ended this tact with 200000 money, 940 electricity, 970 oil, 9800 steal.\n" +
//                "********** Tact 32: **********\n" +
//                "Factory ended this tact with 200000 money, 940 electricity, 970 oil, 9800 steal.\n" +
//                "********** Tact 33: **********\n" +
//                "Factory ended this tact with 200000 money, 940 electricity, 970 oil, 9800 steal.\n" +
//                "********** Tact 34: **********\n" +
//                "Factory ended this tact with 200000 money, 940 electricity, 970 oil, 9800 steal.\n" +
//                "********** Tact 35: **********\n" +
//                "Factory ended this tact with 200000 money, 940 electricity, 970 oil, 9800 steal.\n" +
//                "********** Tact 36: **********\n" +
//                "Factory ended this tact with 200000 money, 940 electricity, 970 oil, 9800 steal.\n" +
//                "********** Tact 37: **********\n" +
//                "Factory ended this tact with 200000 money, 940 electricity, 970 oil, 9800 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): Type \"yes\" to confirm exiting the factory: ";
//        String command = "add contract low motorbike 10000 10\n" +
//                "n 20\n" +
//                "quit\n" +
//                "yes";
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//
//        PrintStream old = System.out;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
//        Factory f = FactoryBuilder.createDefaultFactory().setMinimalPercentProfit(15).getFactory();
//        f.run();
//        in = new ByteArrayInputStream("h\nq".getBytes());
//        System.setIn(in);
//        System.setOut(old);
//        System.out.println(baos.toString());
//        if(baos.toString().contentEquals(rez)){
//            assertTrue(baos.toString().contentEquals(rez));
//        }else {
//            assertSame(baos.toString(), rez);
//        }
//    }
//
//
//    @Test
//    public void TruckTest() {
//        String rez = "\n" +
//                "Factory waits for command (type \"help\" to show help): Given contract was added into the Production as pending. Production will try to start it in next tact.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 38: **********\n" +
//                "There is a Contract request for \"Contract of priority LOW for 10 (done 0) TRUCKs\", which cannot be satisfied automatically. Please choose a solution from:\n" +
//                "\t1) Postpone the contract\n" +
//                "\t2) Buy new needed workplaces: Wheel mounting robot, Welding shop, Press shop, Assembly shop, in total cost: 8500\n" +
//                "Type number of your choosen solution (1 - 2): Factory ended this tact with 141500 money, 1000 electricity, 1000 oil, 10000 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 39: **********\n" +
//                "Factory ended this tact with 141500 money, 997 electricity, 998 oil, 8800 steal.\n" +
//                "********** Tact 40: **********\n" +
//                "Factory ended this tact with 141500 money, 991 electricity, 994 oil, 7600 steal.\n" +
//                "********** Tact 41: **********\n" +
//                "Factory ended this tact with 141500 money, 982 electricity, 990 oil, 6400 steal.\n" +
//                "********** Tact 42: **********\n" +
//                "Factory ended this tact with 141500 money, 970 electricity, 986 oil, 5200 steal.\n" +
//                "********** Tact 43: **********\n" +
//                "Factory ended this tact with 141500 money, 956 electricity, 980 oil, 4000 steal.\n" +
//                "********** Tact 44: **********\n" +
//                "Factory ended this tact with 141500 money, 939 electricity, 974 oil, 2800 steal.\n" +
//                "********** Tact 45: **********\n" +
//                "Factory ended this tact with 141500 money, 919 electricity, 968 oil, 1600 steal.\n" +
//                "********** Tact 46: **********\n" +
//                "Factory ended this tact with 141500 money, 898 electricity, 961 oil, 400 steal.\n" +
//                "********** Tact 47: **********\n" +
//                "Factory ended this tact with 135500 money, 876 electricity, 953 oil, 1200 steal.\n" +
//                "********** Tact 48: **********\n" +
//                "Factory ended this tact with 135500 money, 853 electricity, 944 oil, 0 steal.\n" +
//                "********** Tact 49: **********\n" +
//                "Product TRUCK-21 in stage 10 from Contract of priority LOW for 10 (done 0) TRUCKs finished\n" +
//                "Factory ended this tact with 135500 money, 833 electricity, 937 oil, 0 steal.\n" +
//                "********** Tact 50: **********\n" +
//                "Product TRUCK-22 in stage 10 from Contract of priority LOW for 10 (done 1) TRUCKs finished\n" +
//                "Factory ended this tact with 135500 money, 816 electricity, 932 oil, 0 steal.\n" +
//                "********** Tact 51: **********\n" +
//                "Product TRUCK-23 in stage 10 from Contract of priority LOW for 10 (done 2) TRUCKs finished\n" +
//                "Factory ended this tact with 135500 money, 802 electricity, 927 oil, 0 steal.\n" +
//                "********** Tact 52: **********\n" +
//                "Product TRUCK-24 in stage 10 from Contract of priority LOW for 10 (done 3) TRUCKs finished\n" +
//                "Factory ended this tact with 135500 money, 791 electricity, 922 oil, 0 steal.\n" +
//                "********** Tact 53: **********\n" +
//                "Product TRUCK-25 in stage 10 from Contract of priority LOW for 10 (done 4) TRUCKs finished\n" +
//                "Factory ended this tact with 135500 money, 782 electricity, 919 oil, 0 steal.\n" +
//                "********** Tact 54: **********\n" +
//                "Product TRUCK-26 in stage 10 from Contract of priority LOW for 10 (done 5) TRUCKs finished\n" +
//                "Factory ended this tact with 135500 money, 776 electricity, 916 oil, 0 steal.\n" +
//                "********** Tact 55: **********\n" +
//                "Product TRUCK-27 in stage 10 from Contract of priority LOW for 10 (done 6) TRUCKs finished\n" +
//                "Factory ended this tact with 135500 money, 773 electricity, 913 oil, 0 steal.\n" +
//                "********** Tact 56: **********\n" +
//                "Product TRUCK-28 in stage 10 from Contract of priority LOW for 10 (done 7) TRUCKs finished\n" +
//                "Factory ended this tact with 135500 money, 771 electricity, 911 oil, 0 steal.\n" +
//                "********** Tact 57: **********\n" +
//                "Product TRUCK-29 in stage 10 from Contract of priority LOW for 10 (done 8) TRUCKs finished\n" +
//                "Factory ended this tact with 135500 money, 770 electricity, 910 oil, 0 steal.\n" +
//                "********** Tact 58: **********\n" +
//                "Product TRUCK-30 in stage 10 from Contract of priority LOW for 10 (done 9) TRUCKs finished\n" +
//                "Contract of priority LOW for 10 (done 10) TRUCKs finished\n" +
//                "Factory ended this tact with 185500 money, 770 electricity, 910 oil, 0 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): Type \"yes\" to confirm exiting the factory: ";
//        String command = "add contract low truck 10000 10\n" +
//                "n\n" +
//                "2\n" +
//                "n 20\n" +
//                "quit\n" +
//                "yes";
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//
//        PrintStream old = System.out;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
//        Factory f = FactoryBuilder.createDefaultFactory().setMinimalPercentProfit(15).getFactory();
//        f.run();
//        in = new ByteArrayInputStream("h\nq".getBytes());
//        System.setIn(in);
//        System.setOut(old);
//        System.out.println(baos.toString());
//        if(baos.toString().contentEquals(rez)){
//            assertTrue(baos.toString().contentEquals(rez));
//        }else {
//            assertSame(baos.toString(), rez);
//        }
//    }
//
//
//    @Test
//    public void BusTest() {
//        String rez = "\n" +
//                "Factory waits for command (type \"help\" to show help): Given contract was added into the Production as pending. Production will try to start it in next tact.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 80: **********\n" +
//                "There is a Contract request for \"Contract of priority LOW for 10 (done 0) BUSs\", which cannot be satisfied automatically. Please choose a solution from:\n" +
//                "\t1) Postpone the contract\n" +
//                "\t2) Buy new needed workplaces: Press shop, Interior setting, Glass mounting robot, in total cost: 5000\n" +
//                "Type number of your choosen solution (1 - 2): Factory ended this tact with 145000 money, 1000 electricity, 1000 oil, 10000 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 81: **********\n" +
//                "Factory ended this tact with 145000 money, 997 electricity, 998 oil, 9000 steal.\n" +
//                "********** Tact 82: **********\n" +
//                "Factory ended this tact with 145000 money, 991 electricity, 994 oil, 8000 steal.\n" +
//                "********** Tact 83: **********\n" +
//                "Factory ended this tact with 145000 money, 982 electricity, 990 oil, 7000 steal.\n" +
//                "********** Tact 84: **********\n" +
//                "Factory ended this tact with 145000 money, 971 electricity, 984 oil, 6000 steal.\n" +
//                "********** Tact 85: **********\n" +
//                "Factory ended this tact with 145000 money, 957 electricity, 978 oil, 5000 steal.\n" +
//                "********** Tact 86: **********\n" +
//                "Factory ended this tact with 145000 money, 941 electricity, 972 oil, 4000 steal.\n" +
//                "********** Tact 87: **********\n" +
//                "Factory ended this tact with 145000 money, 923 electricity, 966 oil, 3000 steal.\n" +
//                "********** Tact 88: **********\n" +
//                "Factory ended this tact with 145000 money, 904 electricity, 960 oil, 2000 steal.\n" +
//                "********** Tact 89: **********\n" +
//                "Factory ended this tact with 145000 money, 884 electricity, 954 oil, 1000 steal.\n" +
//                "********** Tact 90: **********\n" +
//                "Factory ended this tact with 145000 money, 863 electricity, 947 oil, 0 steal.\n" +
//                "********** Tact 91: **********\n" +
//                "Product BUS-41 in stage 10 from Contract of priority LOW for 10 (done 0) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 845 electricity, 942 oil, 0 steal.\n" +
//                "********** Tact 92: **********\n" +
//                "Product BUS-42 in stage 10 from Contract of priority LOW for 10 (done 1) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 830 electricity, 939 oil, 0 steal.\n" +
//                "********** Tact 93: **********\n" +
//                "Product BUS-43 in stage 10 from Contract of priority LOW for 10 (done 2) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 818 electricity, 936 oil, 0 steal.\n" +
//                "********** Tact 94: **********\n" +
//                "Product BUS-44 in stage 10 from Contract of priority LOW for 10 (done 3) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 808 electricity, 935 oil, 0 steal.\n" +
//                "********** Tact 95: **********\n" +
//                "Product BUS-45 in stage 10 from Contract of priority LOW for 10 (done 4) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 801 electricity, 934 oil, 0 steal.\n" +
//                "********** Tact 96: **********\n" +
//                "Product BUS-46 in stage 10 from Contract of priority LOW for 10 (done 5) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 796 electricity, 933 oil, 0 steal.\n" +
//                "********** Tact 97: **********\n" +
//                "Product BUS-47 in stage 10 from Contract of priority LOW for 10 (done 6) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 793 electricity, 932 oil, 0 steal.\n" +
//                "********** Tact 98: **********\n" +
//                "Product BUS-48 in stage 10 from Contract of priority LOW for 10 (done 7) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 791 electricity, 931 oil, 0 steal.\n" +
//                "********** Tact 99: **********\n" +
//                "Product BUS-49 in stage 10 from Contract of priority LOW for 10 (done 8) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 790 electricity, 930 oil, 0 steal.\n" +
//                "********** Tact 100: **********\n" +
//                "Product BUS-50 in stage 10 from Contract of priority LOW for 10 (done 9) BUSs finished\n" +
//                "Contract of priority LOW for 10 (done 10) BUSs finished\n" +
//                "Factory ended this tact with 195000 money, 790 electricity, 930 oil, 0 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): Type \"yes\" to confirm exiting the factory: ";
//        String command = "add contract low BUS 10000 10\n" +
//                "n\n" +
//                "2\n" +
//                "n 20\n" +
//                "quit\n" +
//                "yes";
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//
//        PrintStream old = System.out;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
//        Factory f = FactoryBuilder.createDefaultFactory().setMinimalPercentProfit(15).getFactory();
//        f.run();
//        in = new ByteArrayInputStream("h\nq".getBytes());
//        System.setIn(in);
//        System.setOut(old);
//        System.out.println(baos.toString());
//        if(baos.toString().contentEquals(rez)){
//            assertTrue(baos.toString().contentEquals(rez));
//        }else {
//            assertSame(baos.toString(), rez);
//        }
//    }
//
//
//    @Test
//    public void CaravanTest() {
//        String rez = "\n" +
//                "Factory waits for command (type \"help\" to show help): Given contract was added into the Production as pending. Production will try to start it in next tact.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 59: **********\n" +
//                "There is a Contract request for \"Contract of priority LOW for 10 (done 0) BUSs\", which cannot be satisfied automatically. Please choose a solution from:\n" +
//                "\t1) Postpone the contract\n" +
//                "\t2) Buy new needed workplaces: Press shop, Interior setting, Glass mounting robot, in total cost: 5000\n" +
//                "Type number of your choosen solution (1 - 2): Factory ended this tact with 145000 money, 1000 electricity, 1000 oil, 10000 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 60: **********\n" +
//                "Factory ended this tact with 145000 money, 997 electricity, 998 oil, 9000 steal.\n" +
//                "********** Tact 61: **********\n" +
//                "Factory ended this tact with 145000 money, 991 electricity, 994 oil, 8000 steal.\n" +
//                "********** Tact 62: **********\n" +
//                "Factory ended this tact with 145000 money, 982 electricity, 990 oil, 7000 steal.\n" +
//                "********** Tact 63: **********\n" +
//                "Factory ended this tact with 145000 money, 971 electricity, 984 oil, 6000 steal.\n" +
//                "********** Tact 64: **********\n" +
//                "Factory ended this tact with 145000 money, 957 electricity, 978 oil, 5000 steal.\n" +
//                "********** Tact 65: **********\n" +
//                "Factory ended this tact with 145000 money, 941 electricity, 972 oil, 4000 steal.\n" +
//                "********** Tact 66: **********\n" +
//                "Factory ended this tact with 145000 money, 923 electricity, 966 oil, 3000 steal.\n" +
//                "********** Tact 67: **********\n" +
//                "Factory ended this tact with 145000 money, 904 electricity, 960 oil, 2000 steal.\n" +
//                "********** Tact 68: **********\n" +
//                "Factory ended this tact with 145000 money, 884 electricity, 954 oil, 1000 steal.\n" +
//                "********** Tact 69: **********\n" +
//                "Factory ended this tact with 145000 money, 863 electricity, 947 oil, 0 steal.\n" +
//                "********** Tact 70: **********\n" +
//                "Product BUS-31 in stage 10 from Contract of priority LOW for 10 (done 0) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 845 electricity, 942 oil, 0 steal.\n" +
//                "********** Tact 71: **********\n" +
//                "Product BUS-32 in stage 10 from Contract of priority LOW for 10 (done 1) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 830 electricity, 939 oil, 0 steal.\n" +
//                "********** Tact 72: **********\n" +
//                "Product BUS-33 in stage 10 from Contract of priority LOW for 10 (done 2) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 818 electricity, 936 oil, 0 steal.\n" +
//                "********** Tact 73: **********\n" +
//                "Product BUS-34 in stage 10 from Contract of priority LOW for 10 (done 3) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 808 electricity, 935 oil, 0 steal.\n" +
//                "********** Tact 74: **********\n" +
//                "Product BUS-35 in stage 10 from Contract of priority LOW for 10 (done 4) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 801 electricity, 934 oil, 0 steal.\n" +
//                "********** Tact 75: **********\n" +
//                "Product BUS-36 in stage 10 from Contract of priority LOW for 10 (done 5) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 796 electricity, 933 oil, 0 steal.\n" +
//                "********** Tact 76: **********\n" +
//                "Product BUS-37 in stage 10 from Contract of priority LOW for 10 (done 6) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 793 electricity, 932 oil, 0 steal.\n" +
//                "********** Tact 77: **********\n" +
//                "Product BUS-38 in stage 10 from Contract of priority LOW for 10 (done 7) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 791 electricity, 931 oil, 0 steal.\n" +
//                "********** Tact 78: **********\n" +
//                "Product BUS-39 in stage 10 from Contract of priority LOW for 10 (done 8) BUSs finished\n" +
//                "Factory ended this tact with 145000 money, 790 electricity, 930 oil, 0 steal.\n" +
//                "********** Tact 79: **********\n" +
//                "Product BUS-40 in stage 10 from Contract of priority LOW for 10 (done 9) BUSs finished\n" +
//                "Contract of priority LOW for 10 (done 10) BUSs finished\n" +
//                "Factory ended this tact with 195000 money, 790 electricity, 930 oil, 0 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): Type \"yes\" to confirm exiting the factory: ";
//        String command = "add contract low BUS 10000 10\n" +
//                "n\n" +
//                "2\n" +
//                "n 20\n" +
//                "quit\n" +
//                "yes";;
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//
//        PrintStream old = System.out;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
//        Factory f = FactoryBuilder.createDefaultFactory().setMinimalPercentProfit(15).getFactory();
//        f.run();
//        in = new ByteArrayInputStream("h\nq".getBytes());
//        System.setIn(in);
//        System.setOut(old);
//        System.out.println(baos.toString());
//        if(baos.toString().contentEquals(rez)){
//            assertTrue(baos.toString().contentEquals(rez));
//        }else {
//            assertSame(baos.toString(), rez);
//        }
//    }
//
//}