
package cz.cvut.k36.omo.factory.control;

import cz.cvut.k36.omo.factory.archive.Archive;
import cz.cvut.k36.omo.factory.archive.ArchiveType;
import cz.cvut.k36.omo.factory.archive.FactoryConfigurationArchive;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.Task;
import cz.cvut.k36.omo.factory.events.DirectorVisitTask;
import cz.cvut.k36.omo.factory.events.MessageToUserTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class Director implements EventChannelAccessible {

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(Director.class.getName());
        
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final FactoryConfigurationArchive factoryConfigurationArchive;
    private final EventChannel eventChannel;
    private Response response;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Director(EventChannel eventChannel) {
        this.factoryConfigurationArchive = (FactoryConfigurationArchive)Archive.getInstance().getSpecialArchive(ArchiveType.FACTORYCONFIGURATION);
        this.eventChannel = eventChannel;
        this.eventChannel.attach(this);
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    @Override
    public void processTask(Task task) {
        if (task instanceof DirectorVisitTask) {
            doAction();
        }
        else {
            LOG.log(Level.WARNING, "{0} received an unexpected task: {1}", new Object[]{this, task});
        }
    }

    @Override
    public void receiveResponse(Response response) {
        this.response = response;
    }

    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    /**
     * Director does an action with a factory - in this case for example prints
     * all current Conveyors in the factory.
     */
    private void doAction(){
        StringBuilder ret = new StringBuilder("Director visits the factory. He checks all current Conveyors in the factory:");
        factoryConfigurationArchive.getConfig().stream()
                .filter(p -> p.getConveyor() != null)
                .forEach(p -> ret.append("\n\t").append(p.getConveyor()));
        // shows visit's info to user
        eventChannel.addTask(new MessageToUserTask(this, Priority.NORMAL, ret.toString()));
    }
}
