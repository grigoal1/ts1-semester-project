/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.k36.omo.factory.production;

/**
 * Exception thorwn when a Workplace crashes down during a production.
 */
class WorkplaceFaultException extends Exception {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final Workplace workplace;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public WorkplaceFaultException(Workplace workplace) {
        super("Fault on " + workplace);
        this.workplace = workplace;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    Workplace getWorkplace() {
        return workplace;
    }
}
