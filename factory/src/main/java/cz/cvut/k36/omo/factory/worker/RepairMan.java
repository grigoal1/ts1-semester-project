
package cz.cvut.k36.omo.factory.worker;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.RepairTask;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.Task;
import cz.cvut.k36.omo.factory.production.Workplace;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Repairs crashed Workplaces. Chooses the most privileged and oldest crashed
 * Workplace to repair. After repairing a Workplace, leaves it to be ready to
 * repair other. 
 */
public class RepairMan implements EventChannelAccessible, Employee {
    
    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static int totalRepairMen = 0;
    private static final Logger LOG = Logger.getLogger(RepairMan.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int repairManID;
    private Workplace workplace = null;
    private final List<Workplace> pendingTasks;
    private int hours = 0;
    private int hoursInTotal = 0;
    private int earnedMoney = 0;
    
    private final EventChannel eventChannel;
    private Response response;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public RepairMan(EventChannel eventChannel) {
        this.repairManID = ++totalRepairMen;
        this.eventChannel = eventChannel;
        this.eventChannel.attach(this);
        this.pendingTasks = new LinkedList<>();
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */

    /**
     * @return unique RepairMan ID which is the ordinal number of RepairMan
     * creation
     */
    public int getRepairManID() {
        return repairManID;
    }
    
    /**
     * Increases number of total hours which the Worker worker and number of
     * hours which the Worker worked since last billing by one.
     */
    @Override
    public void nextTactOfWorkDone() {
        ++hours;
        ++hoursInTotal;
    }
    
    /**
     * @return Number of hours which the Worker worked from last billing
     */
    @Override
    public int getHours() {
        return hours;
    }
    
    /**
     * @return Number of total hours which the Worker worked
     */
    @Override
    public int getHoursInTotal() {
        return hoursInTotal;
    }
    
    /**
     * Returns number of hours which the Worker worked from last billing and sets
     * them to zero. Worked hours got from this calling must be used to compute
     * Worker's bill.
     * 
     * @return Number of hours which the Worker worked from last billing
     */
    @Override
    public int getHoursToBilling() {
        int ret = getHours();
        hours = 0;
        return ret;
    }

    @Override
    public void earnMoney(int money) {
        if (money > 0) {
            earnedMoney += money;
        }
    }
    
    /**
     * Attaches the RepairMan to the given Workplace to repait it. If null is
     * given, RepairMan detaches from previous workplace and ends its repairing.
     * If the RepairMan is attached, it does not accept new workplace.
     * 
     * @param workplace Workplace to be repaired by the RepairMan, or null to
     * detach the RepairMan
     */
    public void beAttachedTo(Workplace workplace) {
        if (isAttached()) {
            // case of ending a repair
            if (workplace == null) {
                this.workplace = null;
                findNewWorkplaceToRepair();
            }
            // else cannot be attached to a new workplace until the previous is repaired
            return;
        }
        this.workplace = workplace;
    }
    
    /**
     * @return Current Workplace which is being repaired by the RepairMan,
     * null if he is not attached at the moment
     */
    public Workplace getCurrentWorkplace() {
        return workplace;
    }
    
    /**
     * @return True if the RepairMan is attached to any Workplace at the moment,
     * false otherwise
     */
    public boolean isAttached() {
        return this.workplace != null;
    }

    @Override
    public void processTask(Task task) {
        if (task instanceof RepairTask) {
            processRepairTask((RepairTask)task);
        }
        else {
            LOG.log(Level.WARNING, "{0} received an unexpected task: {1}", new Object[]{this, task});
        }
    }

    @Override
    public void receiveResponse(Response response) {
        this.response = response;
    }
    
    @Override
    public String toString() {
        return "RepairMan with ID " + repairManID;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    private void processRepairTask(RepairTask rt) {
        LOG.log(Level.FINE, "{0} received a new {1}", new Object[]{this, rt});
        // cannot be attached twice
        if (isAttached()) {
            // adds a task as pending if
            if (!pendingTasks.contains(rt.getWorkplace())) {
                LOG.log(Level.FINEST, "{0} was added as a pending", rt);
                pendingTasks.add(rt.getWorkplace());
            }
            return;
        }
        // attaches to the workplace, if it has no repair man yet
        Workplace workplace = rt.getWorkplace();
        if (workplace != null && workplace.attachRepairMan(this)) {
            LOG.log(Level.FINEST, "{0} was attached to {1}", new Object[]{this, workplace});
        }
    }

    private void findNewWorkplaceToRepair() {
        // higher priorities first
        for (Priority priority : Priority.values()) {
            List<Workplace> cur = pendingTasks.stream()
                    .filter(w -> w.getConveyor().getPriority() == priority)
                    .collect(Collectors.toList());
            for (Workplace workplace : cur) {
                // tries a workplace and removes it from pending
                pendingTasks.remove(workplace);
                // if the repairman was attached, the new workplace was found
                if (workplace.attachRepairMan(this)) {
                    return;
                }
            }
        }
    }
}
