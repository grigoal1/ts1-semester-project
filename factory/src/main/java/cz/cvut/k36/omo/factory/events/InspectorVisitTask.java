
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.control.Inspector;

/**
 * Task for a Inspector to visit the Factory.
 */
public class InspectorVisitTask extends Task {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int specificAction;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public InspectorVisitTask(EventChannelAccessible sender, int specificAction) {
        super(Priority.HIGH, sender, Inspector.class);
        this.specificAction = specificAction;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */

    public int getSpecificAction() {
        return specificAction;
    }
}
