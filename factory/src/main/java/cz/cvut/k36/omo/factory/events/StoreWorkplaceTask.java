
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.supply.Store;
import cz.cvut.k36.omo.factory.production.WorkplaceType;

/**
 * Task for a Store to buy a given Workplace or to receive its price.
 */
public class StoreWorkplaceTask extends Task {
    
    public enum Action {
        BUY,
        GET_PRICE;
    }
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final WorkplaceType workplaceType;
    private final Action action;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public StoreWorkplaceTask(EventChannelAccessible sender, Priority priority, WorkplaceType workplaceType, Action action) {
        super(priority, sender, Store.class);
        this.workplaceType = workplaceType;
        this.action = action;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public WorkplaceType getWorkplaceType() {
        return workplaceType;
    }

    public Action getAction() {
        return action;
    }
    
    @Override
    public String toString() {
        switch (action) {
            case BUY:
                return this.getClass().getSimpleName() + " from " + sender + " to buy " + workplaceType;
            case GET_PRICE:
                return this.getClass().getSimpleName() + " from " + sender + " to get price of " + workplaceType;
        }
        return this.getClass().getSimpleName();
    }
}
