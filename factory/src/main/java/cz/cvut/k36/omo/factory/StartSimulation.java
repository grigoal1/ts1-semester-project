
package cz.cvut.k36.omo.factory;

import cz.cvut.k36.omo.factory.production.MachineType;
import cz.cvut.k36.omo.factory.production.RobotType;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 */
public class StartSimulation {
    
    public static void main(String[] args) {
//        System.out.println(Integer.MAX_VALUE);
//        System.out.println(Integer.MIN_VALUE);
//        Factory.getCurrentTact() = 0;
        Factory.setCurrentTact(0);
        Logger root = Logger.getLogger("");
        root.getHandlers()[0].setLevel(Level.WARNING);  
        root.setLevel(Level.WARNING);
        
        Factory f = FactoryBuilder.createDefaultFactory().getFactory();
//        Factory f = FactoryBuilder.createEmptyFactory().getFactory();
        f.run();
    }
}
