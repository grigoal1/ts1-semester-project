package cz.cvut.k36.omo.factory.archive;

import cz.cvut.k36.omo.factory.Factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class OuttagesArchive implements SpecialArchive{

    static class RepairHistory {
        final int workplaceId ;
        final int startReporting;
        int startRepair = -1;
        int end = -1;
        int time = -1;
        
        public RepairHistory(int workplaceId, int startReporting) {
            this.workplaceId = workplaceId;
            this.startReporting = startReporting;
        }
    }

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(OuttagesArchive.class.getName());
    
    /* ----------------------- INSTANCE'S FIELDS ----------------------- */
    
    private final Map<Integer, RepairHistory> repairTmpHistory;
    private final List<RepairHistory> repairHistory;
    private final List<Integer> workplaces;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    OuttagesArchive(){
        repairTmpHistory = new HashMap<>();
        repairHistory = new ArrayList<>();
        workplaces = new ArrayList<>();
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */


    public void addMachine(int workplaceId) {
        if (!workplaces.contains(workplaceId))
            workplaces.add(workplaceId);
    }

    public void getWorkplaceStatus(int tact){
        for (int wpID :workplaces) {
            AtomicBoolean state = new AtomicBoolean(true);
            repairHistory.stream()
                    .filter(p -> p.workplaceId == wpID)
                    .filter(p -> p.startReporting <= tact)
                    .forEach( p -> state.set(p.end < tact));
            if (repairTmpHistory.containsKey(wpID) && repairTmpHistory.get(wpID).startReporting <= tact) {
                state.set(false);
            }
            System.out.println("WorkplaceID = " + wpID);
            if (state.get()){
                System.out.println("\tWorking condition");
            }else{
                System.out.println("\tNot working condition");
            }
        }

    }
//add contract low car 100000 1000000
    public void getEventInfo(int tact){
        for (int wpID :workplaces) {
            AtomicBoolean state = new AtomicBoolean(true);
            System.out.println("WorkplaceID = " + wpID);
            if (repairHistory.stream().filter(p -> p.workplaceId == wpID && p.startReporting <= tact ).toArray().length > 0) {
                repairHistory.stream()
                        .filter(p -> p.workplaceId == wpID)
                        .filter(p -> p.startReporting <= tact)
                        .forEach( p -> {
                            if (p.end <= tact) {
                                System.out.println("\tStart reporting = " + p.startReporting + ". Start repair = " + p.startRepair + ". End repair = " + p.end + ".");
                            }else if(p.startRepair <= tact){
                                state.set(false);
                                System.out.println("\tStart reporting = " + p.startReporting + ". Start repair = " + p.startRepair + ". Repair in process.");
                            }else{
                                state.set(false);
                                System.out.println("\tStart reporting = " + p.startReporting + ". Waiting for the start of repair");
                            }

                        });
            }
            if (repairTmpHistory.containsKey(wpID) && repairTmpHistory.get(wpID).startReporting <= tact) {
                RepairHistory rh = repairTmpHistory.get(wpID);
                System.out.print("\tStart reporting = " + rh.startReporting);
                if (rh.startRepair >= 0){
                    System.out.print(" Start repair = " + rh.startRepair);
                    if (rh.end >= 0){
                        System.out.print(" End repair = " + rh.end);
                    }else {
                        state.set(false);
                        System.out.println(" Repair in process");
                    }
                }else{
                    state.set(false);
                    System.out.println(" Waiting for the start of repair");
                }

            }
            if (state.get()){
                System.out.println("\tWorking condition");
            }else{
                System.out.println("\tNot working condition");
            }
        }

    }



    public void addStartReporting(int wpId){
        repairTmpHistory.put(wpId, new RepairHistory(wpId, Factory.getCurrentTact()));
    }

    public void addStartRepair(int wpId){
        RepairHistory repair = repairTmpHistory.get(wpId);
        if (repair == null) {
            LOG.warning("Start of repair which has not been added as started received");
            return;
        }
        repair.startRepair = Factory.getCurrentTact();
    }

    public void addEndRepair(int wpId){
        RepairHistory repair = repairTmpHistory.remove(wpId);
        if (repair == null) {
            LOG.warning("End of repair which has not been added as started received");
            return;
        }
        repair.end = Factory.getCurrentTact();
        repair.time = repair.end - repair.startReporting;
        repairHistory.add(repair);
    }

    @Override
    public ArchiveType getType() {
        return ArchiveType.OUTTAGES;
    }

    public int outtagesCount() {
        return repairHistory.size() + repairTmpHistory.size();
    }
    
    public int longestOutage() {
        return repairHistory.stream()
                .mapToInt(p -> p.time)
                .max()
                .orElse(0);
    }

    public int longestOutage(int start, int end) {
        return editRepairHistory(start, end).stream()
                .mapToInt(p -> p.time)
                .max()
                .orElse(0);
    }

    public int shortestOutage() {
        return repairHistory.stream()
                .mapToInt(p -> p.time)
                .min()
                .orElse(0);
    }

    public int shortestOutage(int start, int end) {
        return editRepairHistory(start, end).stream()
                .mapToInt(p -> p.time)
                .min()
                .orElse(0);
    }

    public Double averageOutageTime() {
        return repairHistory.stream()
                .mapToInt(p -> p.time)
                .average()
                .orElse(0);
    }

    public Double averageOutageTime(int start, int end) {
        return editRepairHistory(start, end).stream()
                .mapToInt(p -> p.time)
                .average()
                .orElse(0);
    }

    public Double theAverageWaitingTimeForRepairers() {
        return repairHistory.stream()
                .mapToInt(p -> p.startRepair - p.startReporting)
                .average()
                .orElse(0);
    }

    public Double theAverageWaitingTimeForRepairers(int start, int end) {
        return editRepairHistory(start, end).stream()
                .mapToInt(p -> p.startRepair - p.startReporting)
                .average()
                .orElse(0);
    }

    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    private ArrayList<RepairHistory> editRepairHistory(int start, int end){
        return repairHistory.stream()
                .filter(p -> p.startReporting >= start && p.end <= end)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
