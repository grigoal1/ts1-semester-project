//package cz.cvut.k36.omo.factory.JunitTests;
//
//import cz.cvut.k36.omo.factory.Factory;
//import cz.cvut.k36.omo.factory.FactoryBuilder;
//import cz.cvut.k36.omo.factory.archive.*;
//import org.junit.jupiter.api.Test;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.PrintStream;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class FactoryBuilderTest {
//
//    @Test
//    public void CreateDefaultFactoryTest(){
//        String rez = "\n" +
//                "Factory waits for command (type \"help\" to show help): Given contract was added into the Production as pending. Production will try to start it in next tact.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 1: **********\n" +
//                "Factory ended this tact with 2830000 money, 1000 electricity, 1000 oil, 10000 steal.\n" +
//                "********** Tact 2: **********\n" +
//                "Factory ended this tact with 2830000 money, 997 electricity, 998 oil, 9900 steal.\n" +
//                "********** Tact 3: **********\n" +
//                "Factory ended this tact with 2830000 money, 992 electricity, 994 oil, 9800 steal.\n" +
//                "********** Tact 4: **********\n" +
//                "Factory ended this tact with 2830000 money, 985 electricity, 990 oil, 9700 steal.\n" +
//                "********** Tact 5: **********\n" +
//                "Factory ended this tact with 2830000 money, 975 electricity, 986 oil, 9600 steal.\n" +
//                "********** Tact 6: **********\n" +
//                "Factory ended this tact with 2830000 money, 964 electricity, 981 oil, 9500 steal.\n" +
//                "********** Tact 7: **********\n" +
//                "Factory ended this tact with 2830000 money, 952 electricity, 975 oil, 9400 steal.\n" +
//                "********** Tact 8: **********\n" +
//                "Product CAR-1 in stage 6 from Contract of priority LOW for 10000 (done 0) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 940 electricity, 969 oil, 9300 steal.\n" +
//                "********** Tact 9: **********\n" +
//                "Product CAR-2 in stage 6 from Contract of priority LOW for 10000 (done 1) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 928 electricity, 963 oil, 9200 steal.\n" +
//                "********** Tact 10: **********\n" +
//                "Product CAR-3 in stage 6 from Contract of priority LOW for 10000 (done 2) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 916 electricity, 957 oil, 9100 steal.\n" +
//                "********** Tact 11: **********\n" +
//                "Product CAR-4 in stage 6 from Contract of priority LOW for 10000 (done 3) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 904 electricity, 951 oil, 9000 steal.\n" +
//                "********** Tact 12: **********\n" +
//                "Product CAR-5 in stage 6 from Contract of priority LOW for 10000 (done 4) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 892 electricity, 945 oil, 8900 steal.\n" +
//                "********** Tact 13: **********\n" +
//                "Product CAR-6 in stage 6 from Contract of priority LOW for 10000 (done 5) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 880 electricity, 939 oil, 8800 steal.\n" +
//                "********** Tact 14: **********\n" +
//                "Product CAR-7 in stage 6 from Contract of priority LOW for 10000 (done 6) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 868 electricity, 933 oil, 8700 steal.\n" +
//                "********** Tact 15: **********\n" +
//                "Product CAR-8 in stage 6 from Contract of priority LOW for 10000 (done 7) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 856 electricity, 927 oil, 8600 steal.\n" +
//                "********** Tact 16: **********\n" +
//                "Product CAR-9 in stage 6 from Contract of priority LOW for 10000 (done 8) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 844 electricity, 921 oil, 8500 steal.\n" +
//                "********** Tact 17: **********\n" +
//                "Product CAR-10 in stage 6 from Contract of priority LOW for 10000 (done 9) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 832 electricity, 915 oil, 8400 steal.\n" +
//                "********** Tact 18: **********\n" +
//                "Product CAR-11 in stage 6 from Contract of priority LOW for 10000 (done 10) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 820 electricity, 909 oil, 8300 steal.\n" +
//                "********** Tact 19: **********\n" +
//                "Product CAR-12 in stage 6 from Contract of priority LOW for 10000 (done 11) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 808 electricity, 903 oil, 8200 steal.\n" +
//                "********** Tact 20: **********\n" +
//                "Product CAR-13 in stage 6 from Contract of priority LOW for 10000 (done 12) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 796 electricity, 897 oil, 8100 steal.\n" +
//                "********** Tact 21: **********\n" +
//                "Product CAR-14 in stage 6 from Contract of priority LOW for 10000 (done 13) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 784 electricity, 891 oil, 8000 steal.\n" +
//                "********** Tact 22: **********\n" +
//                "Product CAR-15 in stage 6 from Contract of priority LOW for 10000 (done 14) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 772 electricity, 885 oil, 7900 steal.\n" +
//                "********** Tact 23: **********\n" +
//                "Product CAR-16 in stage 6 from Contract of priority LOW for 10000 (done 15) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 760 electricity, 879 oil, 7800 steal.\n" +
//                "********** Tact 24: **********\n" +
//                "Product CAR-17 in stage 6 from Contract of priority LOW for 10000 (done 16) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 748 electricity, 873 oil, 7700 steal.\n" +
//                "********** Tact 25: **********\n" +
//                "Product CAR-18 in stage 6 from Contract of priority LOW for 10000 (done 17) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 736 electricity, 867 oil, 7600 steal.\n" +
//                "********** Tact 26: **********\n" +
//                "Product CAR-19 in stage 6 from Contract of priority LOW for 10000 (done 18) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 724 electricity, 861 oil, 7500 steal.\n" +
//                "********** Tact 27: **********\n" +
//                "Product CAR-20 in stage 6 from Contract of priority LOW for 10000 (done 19) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 712 electricity, 855 oil, 7400 steal.\n" +
//                "********** Tact 28: **********\n" +
//                "Product CAR-21 in stage 6 from Contract of priority LOW for 10000 (done 20) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 700 electricity, 849 oil, 7300 steal.\n" +
//                "********** Tact 29: **********\n" +
//                "Product CAR-22 in stage 6 from Contract of priority LOW for 10000 (done 21) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 688 electricity, 843 oil, 7200 steal.\n" +
//                "********** Tact 30: **********\n" +
//                "Product CAR-23 in stage 6 from Contract of priority LOW for 10000 (done 22) CARs finished\n" +
//                "Factory ended this tact with 2830000 money, 676 electricity, 837 oil, 7100 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): Type \"yes\" to confirm exiting the factory: ";
//        String command = "add contract low car 546 10000\n" +
//                "n 30\n" +
//                "quit\n" +
//                "yes";
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//
//        PrintStream old = System.out;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
////        System.setOut(old);
//
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        root.setLevel(Level.WARNING);
//
//        Factory f = FactoryBuilder.createDefaultFactory().getFactory();
//        f.run();
//
//        if(baos.toString().contentEquals(rez)){
//            assertTrue(baos.toString().contentEquals(rez));
//        }else {
//            assertSame(baos.toString(), rez);
//        }
//    }
//
//    @Test
//    public void CreateEmptyFactoryTest(){
//        String rez = "\n" +
//                "Factory waits for command (type \"help\" to show help): Given contract was added into the Production as pending. Production will try to start it in next tact.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 61: **********\n" +
//                "There is a Contract request for \"Contract of priority LOW for 10 (done 0) CARs\", which cannot be satisfied automatically. Please choose a solution from:\n" +
//                "\t1) Postpone the contract\n" +
//                "\t2) Buy new needed workplaces: Coloring robot, Press shop, Wheel mounting robot, Engine plant, Interior setting, Assembly shop, in total cost: 12000\n" +
//                "Type number of your choosen solution (1 - 2): The factory needs 1770 of money to work on.\n" +
//                "Give at least the needed amount of money to the factory (type 1770 or higner number)\n" +
//                "or let the factory bankrupt and destroy (type \"bankrupt\"): Factory ended this tact with 10730 money, 0 electricity, 0 oil, 0 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 62: **********\n" +
//                "Factory ended this tact with 4730 money, 997 electricity, 998 oil, 900 steal.\n" +
//                "********** Tact 63: **********\n" +
//                "Factory ended this tact with 4730 money, 992 electricity, 994 oil, 800 steal.\n" +
//                "********** Tact 64: **********\n" +
//                "Factory ended this tact with 4730 money, 985 electricity, 990 oil, 700 steal.\n" +
//                "********** Tact 65: **********\n" +
//                "Factory ended this tact with 4730 money, 975 electricity, 986 oil, 600 steal.\n" +
//                "********** Tact 66: **********\n" +
//                "Factory ended this tact with 4730 money, 964 electricity, 981 oil, 500 steal.\n" +
//                "********** Tact 67: **********\n" +
//                "Factory ended this tact with 4730 money, 952 electricity, 975 oil, 400 steal.\n" +
//                "********** Tact 68: **********\n" +
//                "Product CAR-30 in stage 6 from Contract of priority LOW for 10 (done 0) CARs finished\n" +
//                "Factory ended this tact with 4730 money, 940 electricity, 969 oil, 300 steal.\n" +
//                "********** Tact 69: **********\n" +
//                "Product CAR-31 in stage 6 from Contract of priority LOW for 10 (done 1) CARs finished\n" +
//                "Factory ended this tact with 4730 money, 928 electricity, 963 oil, 200 steal.\n" +
//                "********** Tact 70: **********\n" +
//                "Product CAR-32 in stage 6 from Contract of priority LOW for 10 (done 2) CARs finished\n" +
//                "Factory ended this tact with 4730 money, 916 electricity, 957 oil, 100 steal.\n" +
//                "********** Tact 71: **********\n" +
//                "Product CAR-33 in stage 6 from Contract of priority LOW for 10 (done 3) CARs finished\n" +
//                "Factory ended this tact with 4730 money, 904 electricity, 951 oil, 0 steal.\n" +
//                "********** Tact 72: **********\n" +
//                "Product CAR-34 in stage 6 from Contract of priority LOW for 10 (done 4) CARs finished\n" +
//                "Factory ended this tact with 4730 money, 895 electricity, 947 oil, 0 steal.\n" +
//                "********** Tact 73: **********\n" +
//                "Product CAR-35 in stage 6 from Contract of priority LOW for 10 (done 5) CARs finished\n" +
//                "Factory ended this tact with 4730 money, 888 electricity, 945 oil, 0 steal.\n" +
//                "********** Tact 74: **********\n" +
//                "Product CAR-36 in stage 6 from Contract of priority LOW for 10 (done 6) CARs finished\n" +
//                "Factory ended this tact with 4730 money, 883 electricity, 943 oil, 0 steal.\n" +
//                "********** Tact 75: **********\n" +
//                "Product CAR-37 in stage 6 from Contract of priority LOW for 10 (done 7) CARs finished\n" +
//                "Factory ended this tact with 4730 money, 881 electricity, 941 oil, 0 steal.\n" +
//                "********** Tact 76: **********\n" +
//                "Product CAR-38 in stage 6 from Contract of priority LOW for 10 (done 8) CARs finished\n" +
//                "Factory ended this tact with 4730 money, 880 electricity, 940 oil, 0 steal.\n" +
//                "********** Tact 77: **********\n" +
//                "Product CAR-39 in stage 6 from Contract of priority LOW for 10 (done 9) CARs finished\n" +
//                "Contract of priority LOW for 10 (done 10) CARs finished\n" +
//                "Factory ended this tact with 7460 money, 880 electricity, 940 oil, 0 steal.\n" +
//                "********** Tact 78: **********\n" +
//                "Factory ended this tact with 7460 money, 880 electricity, 940 oil, 0 steal.\n" +
//                "********** Tact 79: **********\n" +
//                "Factory ended this tact with 7460 money, 880 electricity, 940 oil, 0 steal.\n" +
//                "********** Tact 80: **********\n" +
//                "Factory ended this tact with 7460 money, 880 electricity, 940 oil, 0 steal.\n" +
//                "********** Tact 81: **********\n" +
//                "Factory ended this tact with 7460 money, 880 electricity, 940 oil, 0 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): Type \"yes\" to confirm exiting the factory: ";
//        String command = "add contract low car 546 10\n" +
//                "n\n" +
//                "2\n" +
//                "20000\n" +
//                "n 20\n" +
//                "quit\n" +
//                "yes";
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//
//        PrintStream old = System.out;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
//
//
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        root.setLevel(Level.WARNING);
//
//        Factory f = FactoryBuilder.createEmptyFactory().getFactory();
//        f.run();
//
////        System.setOut(old);
//
//        if(baos.toString().contentEquals(rez)){
//            assertTrue(baos.toString().contentEquals(rez));
//        }else {
//            assertSame(baos.toString(), rez);
//        }
//    }
//
//    @Test
//    public void CreateFullFactoryTest(){
//        String rez = "\n" +
//                "Factory waits for command (type \"help\" to show help): Minimal price for a CAR is 546.\n" +
//                "Given contract was not added into the Production. It was unfavorable.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): ********** Tact 31: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 32: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 33: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 34: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 35: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 36: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 37: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 38: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 39: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 40: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 41: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 42: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 43: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 44: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 45: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 46: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 47: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 48: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 49: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 50: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 51: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 52: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 53: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 54: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 55: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 56: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 57: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 58: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 59: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "********** Tact 60: **********\n" +
//                "Factory ended this tact with 100000 money, 5000 electricity, 5000 oil, 50000 steal.\n" +
//                "\n" +
//                "Factory waits for command (type \"help\" to show help): Type \"yes\" to confirm exiting the factory: ";
//        String command = "add contract low car 10 10\n" +
//                "n 30\n" +
//                "quit\n" +
//                "yes";
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//
//        PrintStream old = System.out;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
////        System.setOut(old);
//
//
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        root.setLevel(Level.WARNING);
//
//        Factory f = FactoryBuilder.createFullFactory().getFactory();
//        f.run();
//
//        ConsumptionArchive consumptionArchive = (ConsumptionArchive) Archive.getInstance().getSpecialArchive(ArchiveType.CONSUMPTION);
////        System.out.println("________");
////        System.out.println(consumptionArchive.);
//
//
//        if(baos.toString().contentEquals(rez)){
//            assertTrue(baos.toString().contentEquals(rez));
//        }else {
//            assertSame(baos.toString(), rez);
//        }
//    }
//}