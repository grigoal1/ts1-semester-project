
package cz.cvut.k36.omo.factory.events;

/**
 * Response to a StoreResourceTask holding received resources and their price.
 */
public class StoreResourceResponse extends Response {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int resources;
    private final int price;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public StoreResourceResponse(EventChannelAccessible sender, StoreResourceTask previousTask, int resources, int price) {
        super(sender, previousTask);
        this.resources = resources;
        this.price = price;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * @return amount of bought resources, 0 if purchase failed or if the previous task was only price getting
     */
    public int getResource() {
        return resources;
    }

    public int getPrice() {
        return price;
    }
    
    @Override
    public String toString() {
        if (resources > 0) {
            return this.getClass().getSimpleName() + " returning " + resources + " of " + ((StoreResourceTask)previousTask).getResourceType()
                    + " from " + sender + " to " + previousTask.sender + " for price " + price;
        }
        return this.getClass().getSimpleName() + " returning " + ((StoreResourceTask)previousTask).getResourceType() + "'s price: " + price;
    }
}
