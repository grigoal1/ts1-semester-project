
package cz.cvut.k36.omo.factory.production;

/**
 * Type of Robot and its general characteristics.
 */
public enum RobotType implements WorkplaceType {
    COLORING(2000, 1, 1, 100, 3, "Coloring robot"),
    GLASS_MOUNTING(1000, 1, 0, 200, 2, "Glass mounting robot"),
    WHEEL_MOUNTING(1000, 1, 1, 150, 2, "Wheel mounting robot");
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int price;
    private final int electricityConsumption;
    private final int oilConsumption;
    private final int tactsToFault;
    private final int tactsForRepair;
    private final String typeString;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    private RobotType(int price, int electricityConsumption, 
                        int oilConsumption, int tactsToFault,
                        int tactsForRepair, String typeString) {
        this.price = price;
        this.electricityConsumption = electricityConsumption;
        this.oilConsumption = oilConsumption;
        this.tactsToFault = tactsToFault;
        this.tactsForRepair = tactsForRepair;
        this.typeString = typeString;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    @Override
    public int getBasePrice() {
        return price;
    }
    
    @Override
    public int getElectricityConsumption() {
        return electricityConsumption;
    }

    @Override
    public int getOilConsumption() {
        return oilConsumption;
    }

    @Override
    public int getTactsToFault() {
        return tactsToFault;
    }

    @Override
    public int getTactsForRepair() {
        return tactsForRepair;
    }

    @Override
    public String toString() {
        return typeString;
    }

    @Override
    public boolean isMachineType() {
        return false;
    }

    @Override
    public boolean isRobotType() {
        return true;
    }
}
