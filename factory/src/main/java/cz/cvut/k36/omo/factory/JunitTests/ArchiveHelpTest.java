//package cz.cvut.k36.omo.factory.JunitTests;
//
//import cz.cvut.k36.omo.factory.Factory;
//import cz.cvut.k36.omo.factory.FactoryBuilder;
//import cz.cvut.k36.omo.factory.archive.*;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.InputStream;
//import java.io.PrintStream;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class ArchiveHelpTest {
//    @BeforeAll
//    public static void StartFactory(){
//        String command = "help\n" +
//                "quit\n" +
//                "yes\n";
//        PrintStream old = System.out;
//        InputStream oldIn = System.in;
//        Logger root = Logger.getLogger("");
//        root.getHandlers()[0].setLevel(Level.OFF);
//        ByteArrayInputStream in = new ByteArrayInputStream(command.getBytes());
//        System.setIn(in);
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PrintStream ps = new PrintStream(baos);
//        System.setOut(ps);
//        Factory f = FactoryBuilder.createDefaultFactory().setMinimalPercentProfit(15).getFactory();
//        f.run();
//        System.setOut(old);
//        System.setIn(oldIn);
//
//    }
//
//    @Test
//    public void EventArchiveTypeTest(){
//        EventArchive eventArchive = (EventArchive) Archive.getInstance().getSpecialArchive(ArchiveType.EVENT);
//        assertSame(eventArchive.getType(), ArchiveType.EVENT);
//    }
//
//    @Test
//    public void EventArchiveTest(){
//        EventArchive eventArchive = (EventArchive) Archive.getInstance().getSpecialArchive(ArchiveType.EVENT);
//        assertEquals(21, eventArchive.getEvents().size());
//    }
//
//
//    @Test
//    public void FactoryConfigurationArchiveTypeTest(){
//        FactoryConfigurationArchive factoryConfigurationArchive = (FactoryConfigurationArchive) Archive.getInstance().getSpecialArchive(ArchiveType.FACTORYCONFIGURATION);
//        assertSame(factoryConfigurationArchive.getType(), ArchiveType.FACTORYCONFIGURATION);
//    }
//
//    @Test
//    public void FactoryConfigurationArchiveTest(){
//        FactoryConfigurationArchive factoryConfigurationArchive = (FactoryConfigurationArchive) Archive.getInstance().getSpecialArchive(ArchiveType.FACTORYCONFIGURATION);
//        assertEquals(0, factoryConfigurationArchive.getConfig().size());
//    }
//
//
//    @Test
//    public void ConsumptionArchiveTypeTest(){
//        ConsumptionArchive consumptionArchive = (ConsumptionArchive) Archive.getInstance().getSpecialArchive(ArchiveType.CONSUMPTION);
//        assertSame(consumptionArchive.getType(), ArchiveType.CONSUMPTION);
//    }
//
//    @Test
//    public void ConsumptionArchiveTest(){
//        ConsumptionArchive consumptionArchive = (ConsumptionArchive) Archive.getInstance().getSpecialArchive(ArchiveType.CONSUMPTION);
//        assertEquals(consumptionArchive.totalUseOfElectricity(), 0);
//        assertEquals(consumptionArchive.totalUseOfOil(), 0);
//    }
//
//
//    @Test
//    public void OuttagesArchiveArchiveTypeTest(){
//        OuttagesArchive outtagesArchive = (OuttagesArchive) Archive.getInstance().getSpecialArchive(ArchiveType.OUTTAGES);
//        assertSame(outtagesArchive.getType(), ArchiveType.OUTTAGES);
//    }
//
//    @Test
//    public void OuttagesArchiveArchiveTest(){
//        OuttagesArchive outtagesArchive = (OuttagesArchive) Archive.getInstance().getSpecialArchive(ArchiveType.OUTTAGES);
//        assertEquals(outtagesArchive.longestOutage(), 0);
//        assertEquals(outtagesArchive.averageOutageTime(), 0);
//        assertEquals(outtagesArchive.shortestOutage(), 0);
//        assertEquals(outtagesArchive.theAverageWaitingTimeForRepairers(), 0);
//    }
//
//
//}