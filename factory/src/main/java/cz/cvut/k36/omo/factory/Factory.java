
package cz.cvut.k36.omo.factory;

import cz.cvut.k36.omo.factory.archive.Archive;
import cz.cvut.k36.omo.factory.archive.ArchiveType;
import cz.cvut.k36.omo.factory.archive.OuttagesArchive;
import cz.cvut.k36.omo.factory.contract.ConflictSolution;
import cz.cvut.k36.omo.factory.contract.Contract;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.ContractConflictResponse;
import cz.cvut.k36.omo.factory.events.ContractConflictTask;
import cz.cvut.k36.omo.factory.events.DirectorVisitTask;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.InspectorVisitTask;
import cz.cvut.k36.omo.factory.events.MessageToUserTask;
import cz.cvut.k36.omo.factory.events.MoneyNeededResponse;
import cz.cvut.k36.omo.factory.events.MoneyNeededTask;
import cz.cvut.k36.omo.factory.events.NewContractResponse;
import cz.cvut.k36.omo.factory.events.NewContractTask;
import cz.cvut.k36.omo.factory.events.StockResourceTask;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.StockResourceResponse;
import cz.cvut.k36.omo.factory.events.Task;
import cz.cvut.k36.omo.factory.product.ProductType;
import cz.cvut.k36.omo.factory.production.Production;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Factory'input communication with the user by stdin/stdout terminal.
 */
public class Factory implements EventChannelAccessible {

    /* ----------------------- STATIC FIELDS ----------------------- */

    private static int currentTact = 0;

    private static final Logger LOG = Logger.getLogger(Factory.class.getName());
    private static final String NEXT_COMMAND_SHORT = "n";
    private static final String NEXT_COMMAND_LONG = "next";
    private static final String HELP_COMMAND = "help";
    private static final String EXIT_COMMAND = "quit";
    private static final String CONTRACT_COMMAND = "add contract";
    private static final String ADD_MONEY_COMMAND = "add money";
    private static final String REPORT_COMMAND = "report";
    private static final String VISIT_COMMAND = "visit";
    private static final String YES_COMMAND = "yes";
    private static final String CONDITION_COMMAND = "condition";
    private static final String REPAIR_INFO_COMMAND = "repair info";
    private static final String HELP_TEXT = "Use these commands to control the factory (note that commands are NOT case sensitive):\n"
        + "\"n [<NUMBER> [postpone]]\", \"next [<NUMBER> [postpone]]\"\n\tNext NUMBER steps of processing contracts in the factory is done, if NUMBER is not set, "
        + "1 step is done. NUMBER should be a positive integer. If \"postpone\" is set as third parameter, all contract conflicts will be solved by postponing. "
        + "This command ends adding commands in the current tact and starts processing in the factory\n"
        + "\"add contract <PRIORITY> <TYPE> <PRICE> <AMOUNT>\"\n\tA new contract for the factory is declared. PRIORITY should be string"
        + " LOW/NORMAL/HIGH, TYPE should be CAR/MOTORBIKE/TRUCK/BUS/CARAVAN, PRICE and AMOUNT should be positive integer values\n"
        + "\"add money <AMOUNT>\"\n\tFactory receives given amount of money. MONEY should be a positive integer value\n"
        + "\"quit\"\n\tFactory is exited (that means destoyed, all progress is lost)\n"
        + "\"report <TYPE> [<START> <END>]\"\n\tGiven TYPE of report is created. TYPE should be CONFIGURATION/EVENT/CONSUMPTION/OUTTAGES. If START and END are given, "
        + "only data from tacts number START to END are processed. START and END should be integer between 1 to current tact number\n"
        + "\"visit <VISITOR>\"\n\tGiven VISITOR visits the factory. VISITOR should be \"DIRECTOR\" or \"INSPECTOR ID\", where ID is an ID of a conveyor to be inspected\n"
        + "\"condition [<tact>]\"\n\tWriting out the condition of all equipment\n"
        + "\"repair info [<tact>]\"\n\tWrites repair history of all equipment\n";

    /* ----------------------- STATIC METHODS ----------------------- */

    public static int getCurrentTact() {
        return currentTact;
    }

    /* ----------------------- INSTANCES' FIELDS ----------------------- */

    private final EventChannel eventChannel;
    private Response response;

    private final Production production;
    
    private boolean autopostpone;
    private Scanner mainSc;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */

    public Factory(Production production, EventChannel eventChannel) {
        this.eventChannel = eventChannel;
        this.eventChannel.attach(this);
        this.production = production;
        this.mainSc = new Scanner(System.in);
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */

    /**
     * Starts a factory. Factory waits for general commands from stdin.
     */
    public void run() {
        String input;
        while (true) {
            int numberOfNextTacts = 1;
            autopostpone = false;
            while (true) {
                System.out.print("\nFactory waits for command (type \"help\" to show help): ");
                input = mainSc.nextLine().toLowerCase();
                if (isNextTactCommand(input)) {
                    int tacts = handleNextTactCommand(input);
                    if (tacts == 0) {
                        handleInvalidCommand(input);
                    }
                    else {
                        numberOfNextTacts = tacts;
                        break;
                    }
                }
                else if (isHelpCommand(input)) {
                    handleHelpCommand();
                }
                else if (isExitCommand(input)) {
                    if (exitConfirmed()) {
                        return;
                    }
                }
                else if (isContractCommand(input)) {
                    handleContractCommand(input);
                }
                else if (isAddMoneyCommand(input)) {
                    handleAddMoneyCommand(input);
                }
                else if (isReportCommand(input)) {
                    handleReportCommand(input);
                }
                else if (isVisitCommand(input)) {
                    handleVisitCommand(input);
                }
                else if(isRepairInfoCommand(input)){
                    handleRepairInfoCommand(input);
                }
                else if(isConditionCommand(input)){
                    handleConditionCommand(input);
                }
                else {
                    handleInvalidCommand(input);
                }
            }

            for (int i = 0; i < numberOfNextTacts; ++i) {
                ++currentTact;
                LOG.log(Level.INFO, "Tact {0} started", currentTact);
                System.out.println("********** Tact " + currentTact + ": **********");
                production.nextTact();
                if (currentTact % 160 == 0) {
                    LOG.log(Level.INFO, "Tact {0} is paying off", currentTact);
                    production.accountingTact();
                }
                showResourceAmount();
                LOG.log(Level.INFO, "Tact {0} ended", currentTact);
            }
        }
    }

    @Override
    public void processTask(Task task) {
        if (task instanceof ContractConflictTask) {
            processContractConflictTask((ContractConflictTask)task);
        }
        else if (task instanceof MoneyNeededTask) {
            processMoneyNeededTask((MoneyNeededTask)task);
        }
        else if (task instanceof MessageToUserTask) {
            processMessageToUserTask((MessageToUserTask)task);
        }
        else {
            LOG.log(Level.WARNING, "{0} received an unexpected task: {1}", new Object[]{this, task});
        }
    }

    @Override
    public void receiveResponse(Response response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "Factory";
    }

    /* ----------------------- PRIVATE METHODS ----------------------- */

    private void showResourceAmount() {
        int money = 0;
        int electricity = 0;
        int oil = 0;
        int steal = 0;
        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.MONEY));
        if (response instanceof StockResourceResponse) {
            money = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;
        
        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.ELECTRICITY));
        if (response instanceof StockResourceResponse) {
            electricity = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;
        
        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.OIL));
        if (response instanceof StockResourceResponse) {
            oil = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;
        
        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.STEAL));
        if (response instanceof StockResourceResponse) {
            steal = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;
        
        System.out.println("Factory ended this tact with " + money + " money, " + electricity +
                " electricity, " + oil + " oil, " + steal + " steal.");
    }
    /* ----------------------- HANDLEING OF USER'S COMMANDS ----------------------- */

    private boolean isNextTactCommand(String command) {
        String[] parts = command.split(" ");
        if (parts.length > 3 || parts.length < 1) {
            return false;
        }
        return parts[0].equals(NEXT_COMMAND_SHORT) || parts[0].equals(NEXT_COMMAND_LONG);
    }

    private boolean isHelpCommand(String command) {
        return HELP_COMMAND.equals(command);
    }

    private boolean isExitCommand(String command) {
        return EXIT_COMMAND.equals(command);
    }

    private boolean isContractCommand(String command) {
        return (command.startsWith(CONTRACT_COMMAND) && command.split(" ").length == 6);
    }

    private boolean isAddMoneyCommand(String command) {
        return (command.startsWith(ADD_MONEY_COMMAND) && command.split(" ").length == 3);
    }

    private boolean isYesCommand(String command) {
        return YES_COMMAND.equals(command);
    }

    private boolean isReportCommand(String command) {
        String[] parts = command.split(" ");
        return ((parts.length == 2 || parts.length == 4) && parts[0].equals(REPORT_COMMAND));
    }

    private boolean isVisitCommand(String command) {
        String[] parts = command.split(" ");
        return ((parts.length == 2 || parts.length == 3) && parts[0].equals(VISIT_COMMAND));
    }

    private boolean exitConfirmed() {
        System.out.print("Type \"yes\" to confirm exiting the factory: ");
        String input = mainSc.nextLine().toLowerCase();
        return isYesCommand(input);
    }

    private void handleHelpCommand() {
        System.out.println(HELP_TEXT);
    }

    private boolean isConditionCommand(String s){
        String[] parts = s.split(" ");
        return ((parts.length == 2 || parts.length == 1) && parts[0].equals(CONDITION_COMMAND));
    }
    private boolean isRepairInfoCommand(String s){
        String[] parts = s.split(" ");
        return ((parts.length == 3 || parts.length == 2) && s.startsWith(REPAIR_INFO_COMMAND));
    }

    private void handleConditionCommand(String s){
        String[] parts = s.split(" ");
        if (parts.length == 1) {
            ((OuttagesArchive) (Archive.getInstance().getSpecialArchive(ArchiveType.OUTTAGES))).getWorkplaceStatus(Factory.getCurrentTact());
        }else {
            int tact;
            try {
                tact = Integer.parseInt(parts[1]);
                ((OuttagesArchive) (Archive.getInstance().getSpecialArchive(ArchiveType.OUTTAGES))).getWorkplaceStatus(tact);
            }
            catch (NumberFormatException ex) {
                handleInvalidCommand(s);
            }
        }
    }

    private void handleRepairInfoCommand(String s){
        System.out.println("2");
        String[] parts = s.split(" ");
        if (parts.length == 2) {
            ((OuttagesArchive) (Archive.getInstance().getSpecialArchive(ArchiveType.OUTTAGES))).getEventInfo(Factory.getCurrentTact());
        }else {
            int tact;
            try {
                tact = Integer.parseInt(parts[2]);
                ((OuttagesArchive) (Archive.getInstance().getSpecialArchive(ArchiveType.OUTTAGES))).getEventInfo(tact);
            }
            catch (NumberFormatException ex) {
                handleInvalidCommand(s);
            }
        }
    }

    /**
     * @param command
     * @return Number of tacts to be done or 0 if input was invalid
     */
    private int handleNextTactCommand(String command) {
        String[] parts = command.split(" ");
        if (parts.length == 1) {
            return 1;
        }
        if (parts.length == 2 || parts.length == 3) {
            int num;
            try {
                num = Integer.parseInt(parts[1]);
            }
            catch (NumberFormatException ex) {
                return 0;
            }
            if (parts.length == 3 && parts[2].equals("postpone")) {
                autopostpone = true;
            }
            return num < 1 ? 0 : num;
        }
        return 0;
    }

    private void handleContractCommand(String command) {
        String[] parts = command.split(" ");
        Priority priority = null;
        for (Priority p : Priority.values()) {
            if (p.name().toLowerCase().equals(parts[2])) {
                priority = p;
                break;
            }
        }
        if (priority == null) {
            handleInvalidCommand(command);
            return;
        }
        ProductType type = null;
        for (ProductType pt : ProductType.values()) {
            if (pt.name().toLowerCase().equals(parts[3])) {
                type = pt;
                break;
            }
        }
        if (type == null) {
            handleInvalidCommand(command);
            return;
        }
        int price;
        int amount;
        try {
            price = Integer.parseInt(parts[4]);
            amount = Integer.parseInt(parts[5]);
        }
        catch (NumberFormatException ex) {
            handleInvalidCommand(command);
            return;
        }
        if (price < 1 || amount < 1) {
            handleInvalidCommand(command);
            return;
        }
        Contract contract = new Contract(priority, type, price, amount);
        eventChannel.addTask(new NewContractTask(this, contract));
        if (response instanceof NewContractResponse) {
            if (((NewContractResponse)response).wasContractAdded()) {
                System.out.println("Given contract was added into the Production as pending. Production will try to start it in next tact.");
            }
            else {
                System.out.println("Given contract was not added into the Production. It was unfavorable.");
            }
        }
        else {
            LOG.warning("Factory did not received a response after NewContractTask");
        }
        response = null;
    }

    private void handleAddMoneyCommand(String command) {
        String[] parts = command.split(" ");
        int amount;
        try {
            amount = Integer.parseInt(parts[2]);
        }
        catch (NumberFormatException ex) {
            handleInvalidCommand(command);
            return;
        }
        if (amount < 1) {
            handleInvalidCommand(command);
            return;
        }
        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.MONEY, StockResourceTask.Action.ADD, amount));
        System.out.println(amount + " money added");
    }

    private void handleReportCommand(String command) {
        String[] parts = command.split(" ");
        int start = -1;
        int end = -1;
        if (parts.length == 4) {
            try {
                start = Integer.parseInt(parts[2]);
                end = Integer.parseInt(parts[3]);
            }
            catch (NumberFormatException ex) {
                handleInvalidCommand(command);
                return;
            }
            if (start < 1 || end < 1 || start > currentTact || end > currentTact) {
                handleInvalidCommand(command);
                return;
            }
        }
        switch (parts[1]) {
            case "configuration":
                if (start != -1) {
                    Archive.getInstance().factoryConfigurationReport(start, end);
                }
                else {
                    Archive.getInstance().factoryConfigurationReport();
                }
                break;
            case "event":
                if (start != -1) {
                    Archive.getInstance().eventReport(start, end);
                }
                else {
                    Archive.getInstance().eventReport();
                }
                break;
            case "consumption":
                if (start != -1) {
                    Archive.getInstance().consumptionReport(start, end);
                }
                else {
                    Archive.getInstance().consumptionReport();
                }
                break;
            case "outtages":
                if (start != -1) {
                    Archive.getInstance().outtagesReport(start, end);
                }
                else {
                    Archive.getInstance().outtagesReport();
                }
                break;
            default:
                handleInvalidCommand(command);
        }
    }

    private void handleVisitCommand(String command) {
        String[] parts = command.split(" ");
        if (parts.length == 2 && parts[1].equals("director")) {
            eventChannel.addTask(new DirectorVisitTask(this));
        }
        else if (parts.length == 3 && parts[1].equals("inspector")) {
            try {
                int id = Integer.parseInt(parts[2]);
                eventChannel.addTask(new InspectorVisitTask(this, id));
            }
            catch (NumberFormatException ex) {
                handleInvalidCommand(command);
            }
        }
        else {
            handleInvalidCommand(command);
        }
    }

    private void handleInvalidCommand(String s) {
        System.out.println("Invalid command, please check it and type again.");
    }

    /* ----------------------- PROCESSING TASKS ----------------------- */

    private void processContractConflictTask(ContractConflictTask cct) {
        if (autopostpone) {
            System.out.println("Contract request for \"" + cct.getContract() + "\" was automatically postponed.");
            eventChannel.addResponse(new ContractConflictResponse(this, cct, new ConflictSolution()));
            return;
        }
        List<ConflictSolution> solutions = cct.getSolutions();
        System.out.println("There is a Contract request for \"" + cct.getContract() + "\", which cannot be satisfied automatically. Please choose a solution from:");
        int num = 0;
        for (ConflictSolution cs : solutions) {
            ++num;
            System.out.println("\t" + num + ") " + cs.toString());
        }
        Scanner sc = mainSc;
        int choice = 0;
        while (true) {
            System.out.print("Type number of your choosen solution (1 - " + num + "): ");
            String input = sc.nextLine();
            try {
                choice = Integer.parseInt(input);
            }
            catch (NumberFormatException ex) { }
            if (choice > 0 && choice <= num) {
                // sends response
                eventChannel.addResponse(new ContractConflictResponse(this, cct, solutions.get(choice - 1)));
                break;
            }
        }
    }

    private void processMoneyNeededTask(MoneyNeededTask mnt) {
        System.out.println("The factory needs " + mnt.getAmount() + " of money to work on.");
        Scanner sc = mainSc;
        int choice = -1;
        while (true) {
            System.out.print("Give at least the needed amount of money to the factory (type " + mnt.getAmount() + " or higner number)\nor let the factory bankrupt and destroy (type \"bankrupt\"): ");
            String input = sc.nextLine().toLowerCase();
            if (input.equals("bankrupt")) {
                System.out.println("\nThe factory did not make enough money to survive in our cruel world...");
                System.out.println("It has survived for " + currentTact + " tacts. R.I.P. our factory :(");
                System.exit(0);
            }
            try {
                choice = Integer.parseInt(input);
            }
            catch (NumberFormatException ex) { }
            if (choice >= mnt.getAmount()) {
                // sends response
                eventChannel.addResponse(new MoneyNeededResponse(this, mnt, choice));
                break;
            }
        }
    }

    private void processMessageToUserTask(MessageToUserTask mtut) {
        System.out.println(mtut.getMessage());
    }

    public static void setCurrentTact(int currentTact) {
        Factory.currentTact = currentTact;
    }
}
