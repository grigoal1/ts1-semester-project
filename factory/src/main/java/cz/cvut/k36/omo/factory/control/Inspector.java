
package cz.cvut.k36.omo.factory.control;

import cz.cvut.k36.omo.factory.archive.Archive;
import cz.cvut.k36.omo.factory.archive.ArchiveType;
import cz.cvut.k36.omo.factory.archive.FactoryConfigurationArchive;
import cz.cvut.k36.omo.factory.archive.FactoryConfigurationArchive.HistoryConveyor;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.InspectorVisitTask;
import cz.cvut.k36.omo.factory.events.MessageToUserTask;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.Task;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class Inspector implements EventChannelAccessible {

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(Inspector.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
     
    private final FactoryConfigurationArchive factoryConfigurationArchive;
    private final EventChannel eventChannel;
    private Response response;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Inspector(EventChannel eventChannel) {
        this.factoryConfigurationArchive = (FactoryConfigurationArchive)Archive.getInstance().getSpecialArchive(ArchiveType.FACTORYCONFIGURATION);
        this.eventChannel = eventChannel;
        this.eventChannel.attach(this);
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    @Override
    public void processTask(Task task) {
        if (task instanceof InspectorVisitTask) {
            takeASpecificAction(((InspectorVisitTask)task).getSpecificAction());
        }
        else {
            LOG.log(Level.WARNING, "{0} received an unexpected task: {1}", new Object[]{this, task});
        }
    }

    @Override
    public void receiveResponse(Response response) {
        this.response = response;
    }
    
    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    /**
     * inspector does an action with a factory - in this case for example prints
     * the Conveyor with given ID, if it is currently in the factory.
     */
    private void takeASpecificAction(int conveyorId){
        String ret = "Inspector visits the factory. He checks whether there is a conveyor with ID " + conveyorId + " a the moment:\n";
        HistoryConveyor hc = factoryConfigurationArchive.getConfig().stream()
                .filter(p -> p.getConveyor() != null)
                .filter(p -> p.getConveyor().getConveyorId() == conveyorId)
                .findAny()
                .orElse(null);
        if (hc == null) {
            ret += "\tAsked conveyor is not in the factory in the moment.";
        }
        else {
            ret += "It is in the factory: " + hc.getConveyor() + "\nIt contains workplaces:\n" + hc.getConveyor().getWorkplaceStr();
        }
        // shows visit's info to user
        eventChannel.addTask(new MessageToUserTask(this, Priority.NORMAL, ret));
    }
}
