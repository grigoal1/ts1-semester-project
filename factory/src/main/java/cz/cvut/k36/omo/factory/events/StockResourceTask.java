
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import cz.cvut.k36.omo.factory.supply.Stock;

/**
 * Task for a Stock to control Resources in the Stock.
 */
public class StockResourceTask extends Task {
    
    public enum Action {
        GET_AMOUNT,
        USE,
        ADD;
    }
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final ResourceType resourceType;
    private final Action actionType;
    private final int amount;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    /**
     * Creates a task to get amount of the given type of resource in the Stock.
     * 
     * @param sender task's sender
     * @param priority task's priority
     * @param resourceType type of the wanted resource
     */
    public StockResourceTask(EventChannelAccessible sender, Priority priority, ResourceType resourceType) {
        this(sender, priority, resourceType, Action.GET_AMOUNT, 0);
    }
    
    /**
     * Creates a task to USE or ADD given amount of the given type of resource,
     * depending on the given action type.
     * 
     * @param sender task's sender
     * @param priority task's priority
     * @param resourceType type of the wanted resource
     * @param actionType action to perform with wanted resource
     * @param amount amount of wanted resource
     */
    public StockResourceTask(EventChannelAccessible sender, Priority priority, ResourceType resourceType, Action actionType, int amount) {
        super(priority, sender, Stock.class);
        this.resourceType = resourceType;
        this.actionType = actionType;
        this.amount = amount;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * @return Type of the resource which the action is performed with
     */
    public ResourceType getResourceType() {
        return resourceType;
    }

    /**
     * @return Action performed with the resource: GET_AMOUT / USE / ADD
     */
    public Action getActionType() {
        return actionType;
    }

    /**
     * @return Amount of the resource which the action is performed with, it is
     * always 0 when the getActionType() == GET_AMOUNT
     */
    public int getAmount() {
        return amount;
    }
    
    @Override
    public String toString() {
        switch (actionType) {
            case USE:
                return this.getClass().getSimpleName() + " from " + sender + " to use " + amount + " of " + resourceType;
            case ADD:
                return this.getClass().getSimpleName() + " from " + sender + " to add " + amount + " of " + resourceType;
            case GET_AMOUNT:
                return this.getClass().getSimpleName() + " from " + sender + " to get amount of " + resourceType;
        }
        return this.getClass().getSimpleName();
    }
}
