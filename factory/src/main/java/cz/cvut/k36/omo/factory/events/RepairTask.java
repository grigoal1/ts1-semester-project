
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.production.Workplace;
import cz.cvut.k36.omo.factory.worker.RepairMan;

/**
 * Task for a RepairMan containing an ID of the Workplace, which is needed to
 * be repaired.
 */
public class RepairTask extends Task {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final Workplace workplace;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public RepairTask(EventChannelAccessible sender, Priority priority, Workplace workplace) {
        super(priority, sender, RepairMan.class);
        this.workplace = workplace;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public Workplace getWorkplace() {
        return workplace;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " from " + sender + " to repair " + workplace;
    }
}
