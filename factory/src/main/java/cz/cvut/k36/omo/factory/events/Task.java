
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;

/**
 * Task to be done by a recipient class instance.
 */
public abstract class Task {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    protected final Priority priority;
    protected final EventChannelAccessible sender;
    protected final Class<?> recipient;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    protected Task(Priority priority, EventChannelAccessible sender, Class<?> recipient) {
        this.priority = priority;
        this.sender = sender;
        this.recipient = recipient;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public Priority getPriority() {
        return priority;
    }
    
    public EventChannelAccessible getSender() {
        return sender;
    }
    
    public Class<?> getRecipient() {
        return recipient;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " of priority " + priority.name() + " from " + sender + " to " + recipient.getSimpleName();
    }
}