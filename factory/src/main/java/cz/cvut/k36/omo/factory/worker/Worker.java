
package cz.cvut.k36.omo.factory.worker;

import cz.cvut.k36.omo.factory.production.Machine;

/**
 * Worker in the Factory needed for Machines to be able to work.
 */
public class Worker implements Employee {
    
    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static int totalWorkers = 0;
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int workerID;
    private int hours = 0;
    private int hoursInTotal = 0;
    private int earnedMoney = 0;
    private Machine machine = null;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Worker() {
        this.workerID = ++totalWorkers;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */

    /**
     * @return unique Worker ID which is the ordinal number of Worker creation
     */
    public int getWorkerID() {
        return workerID;
    }
    
    /**
     * Increases number of total hours which the Worker worker and number of
     * hours which the Worker worked since last billing by one.
     */
    @Override
    public void nextTactOfWorkDone() {
        ++hours;
        ++hoursInTotal;
    }
    
    /**
     * @return Number of hours which the Worker worked from last billing
     */
    @Override
    public int getHours() {
        return hours;
    }
    
    /**
     * @return Number of total hours which the Worker worked
     */
    @Override
    public int getHoursInTotal() {
        return hoursInTotal;
    }
    
    /**
     * Returns number of hours which the Worker worked from last billing and sets
     * them to zero. Worked hours got from this calling must be used to compute
     * Worker's bill.
     * 
     * @return Number of hours which the Worker worked from last billing
     */
    @Override
    public int getHoursToBilling() {
        int ret = getHours();
        hours = 0;
        return ret;
    }

    @Override
    public void earnMoney(int money) {
        if (money > 0) {
            earnedMoney += money;
        }
    }
    
    /**
     * Attaches the Worker to the given machine.
     * 
     * @param machine Machine for the Worker to be attached to
     */
    public void beAttachedTo(Machine machine) {
        this.machine = machine;
    }
    
    /**
     * @return Current Machine where the Worker works, null if he is not attached
     * at the moment
     */
    public Machine getCurrentMachine() {
        return machine;
    }
    
    /**
     * @return True if the Worker is attached to any Machine at the moment, false
     * otherwise
     */
    public boolean isAttached() {
        return this.machine != null;
    }

    @Override
    public String toString() {
        return "Worker with ID " + workerID;
    }
}
