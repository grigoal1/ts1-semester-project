
package cz.cvut.k36.omo.factory.product;

/**
 * Simple factory for initial creation of Products.
 * Design pattern: singleton, simple factory.
 */
public class PrimeProductFactory {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private static PrimeProductFactory factory;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    private PrimeProductFactory() {
        // private constructor
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public static PrimeProductFactory getInstance() {
        if (factory == null) {
            factory = new PrimeProductFactory();
        }
        return factory;
    }
    
    public Product makeCar() {
        return new Product(ProductType.CAR);
    }
    
    public Product makeMotorbike() {
        return new Product(ProductType.MOTORBIKE);
    }
    
    public Product makeTruck() {
        return new Product(ProductType.TRUCK);
    }
    
    public Product makeBus() {
        return new Product(ProductType.BUS);
    }
    
    public Product makeCaravan() {
        return new Product(ProductType.CARAVAN);
    }
}
