/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.k36.omo.factory.production;

import cz.cvut.k36.omo.factory.archive.Archive;
import cz.cvut.k36.omo.factory.archive.ArchiveType;
import cz.cvut.k36.omo.factory.archive.ConsumptionArchive;
import cz.cvut.k36.omo.factory.archive.OuttagesArchive;
import cz.cvut.k36.omo.factory.product.Product;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import cz.cvut.k36.omo.factory.worker.RepairMan;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Workplace processing a Product. Needs to be placed in a WorkplaceHolder for
 * a Product to be passed by chain of responsibility.
 * 
 * Design pattern: Template method
 */
public abstract class Workplace {
    
    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static int workplaceCount = 0;
    private static final Logger LOG = Logger.getLogger(Workplace.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    protected final int workplaceID;
    protected final WorkplaceType workplaceType;
    protected int tactsToFault;
    protected int broken = 0;
    protected Product curProduct = null;
    protected Conveyor curConveyor = null;
    protected RepairMan repairMan = null;
    protected ConsumptionArchive consumptionArchive;
    protected OuttagesArchive outtagesArchive;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Workplace(WorkplaceType workplaceType) {
        this.workplaceID = ++workplaceCount;
        this.workplaceType = workplaceType;
        this.tactsToFault = workplaceType.getTactsToFault() + new Random().nextInt(workplaceType.getTactsToFault());
        consumptionArchive = (ConsumptionArchive)(Archive.getInstance().getSpecialArchive(ArchiveType.CONSUMPTION));
        outtagesArchive = (OuttagesArchive)(Archive.getInstance().getSpecialArchive(ArchiveType.OUTTAGES));
        outtagesArchive.addMachine(workplaceID);
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public final Conveyor getConveyor() {
        return curConveyor;
    }
    
    public final boolean isInConveyor() {
        return curConveyor != null;
    }
    
    /**
     * Attaches given RepairMan to the Workplace to repair it if broken.
     * The RepairMan is attached until he repairs the Workplace, after that he
     * leaves it.
     * 
     * @param repairMan RepairMan to be attached.
     * @return True if the RepairMan was added, false if there was a RepairMan
     * before, so the new one was not added, or the passed one was null
     */
    public final boolean attachRepairMan(RepairMan repairMan) {
        if (repairMan == null) {
            return false;
        }
        if (this.repairMan == null) {
            this.repairMan = repairMan;
            outtagesArchive.addStartRepair(workplaceID);
            this.repairMan.beAttachedTo(this);
            LOG.log(Level.FINE, "{0} was attached to {1}", new Object[]{repairMan, this});
            return true;
        }
        return false;
    }
    
    public final boolean hasRepairMan() {
        return this.repairMan != null;
    }
    
    public final WorkplaceType getWorkplaceType() {
        return workplaceType;
    }
    
    public final int getWorkplaceID() {
        return workplaceID;
    }
    
    /* ----------------------- PACKAGE METHODS ----------------------- */
    
    abstract boolean isReady();
    
    abstract boolean isMachine();
    
    abstract boolean isRobot();
    
    /**
     * Processes given Product. Workplace can crash down while processing - if so,
     * WorkplaceFaultException is thrown and processing is stopped.
     * 
     * @param product Product to be processed
     * @throws WorkplaceFaultException Thrown when the Workplace crashed down
     * and the RepairMan is needed.
     */
    final void processProduct(Product product) throws WorkplaceFaultException {
        if (product == null) {
            return;
        }
        if (!useResources()) {
            LOG.log(Level.WARNING, "{0} did not get enough needed resources", this);
        }
        curProduct = product;
        LOG.log(Level.FINE, "{0} processes {1}", new Object[]{workplaceType, curProduct});
        product.beProcessedBy(this);
        specialProcessFeature();
        if (--tactsToFault == 0) {
            broken = workplaceType.getTactsForRepair() + new Random().nextInt(workplaceType.getTactsForRepair());
            outtagesArchive.addStartReporting(workplaceID);
            specialBreakDownFeature();
            LOG.log(Level.INFO, "{0} with ID {1} broke down", new Object[]{workplaceType, workplaceID});
            throw new WorkplaceFaultException(this);
        }
    }
    
    /**
     * @return Last processes Product in the Workplace
     */
    final Product getProcessedProduct() {
        Product ret = curProduct;
        curProduct = null;
        return ret;
    }
    
    /**
     * Adds the Workplace into the Conveyor. If null is given as a conveyor,
     * Workplace is now free (isInConveyor() will return false).
     * 
     * @param conveyor New parent Conveyor
     */
    final void addToConveyor(Conveyor conveyor) {
        this.curConveyor = conveyor;
    }
    
    /**
     * Processes the repairing of the Workplace. If the Workplace is repaired
     * now, releases the RepairMan.
     */
    final void processRepair() {
        if (hasRepairMan() && broken > 0) {
            repairMan.nextTactOfWorkDone();
            // If the Workplace is repaired now, releases the RepairMan
            if (--broken == 0) {
                outtagesArchive.addEndRepair(workplaceID);
                this.repairMan.beAttachedTo(null);
                this.repairMan = null;
                // resets counting down of next fault
                this.tactsToFault = workplaceType.getTactsToFault() + new Random().nextInt(workplaceType.getTactsToFault());
            }
        }
    }

    /* ----------------------- PROTECTED METHODS ----------------------- */
    
    /**
     * Adds special funcionality to the processing of the Product by child classes.
     */
    protected abstract void specialProcessFeature();

    /**
     * Adds special funcionality after breaking down by child classes.
     */
    protected abstract void specialBreakDownFeature();

    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    private boolean useResources() {
        int electricityNeeded = workplaceType.getElectricityConsumption();
        if (electricityNeeded > 0) {
            LOG.log(Level.FINEST, "{0} needs {1} electricity", new Object[]{this, electricityNeeded});
            int electricity = curConveyor.useResources(ResourceType.ELECTRICITY, electricityNeeded);
            consumptionArchive.addLogInArchive(this, ResourceType.ELECTRICITY, electricity);
            if (electricity < electricityNeeded) {
                return false;
            }
        }
        int oilNeeded = workplaceType.getOilConsumption();
        if (oilNeeded > 0) {
            LOG.log(Level.FINEST, "{0} needs {1} oil", new Object[]{this, oilNeeded});
            int oil = curConveyor.useResources(ResourceType.OIL, oilNeeded);
            consumptionArchive.addLogInArchive(this, ResourceType.OIL, oil);
            if (oil < oilNeeded) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        return workplaceType + " with ID " + workplaceID;
    }
}
