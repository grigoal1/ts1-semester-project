
package cz.cvut.k36.omo.factory.events;

/**
 * Response to NewContractTask holding information whether the Contract in
 * NewContractTask was added or not.
 */
public class NewContractResponse extends Response {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */

    private final boolean contractAdded;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public NewContractResponse(EventChannelAccessible sender, NewContractTask previousTask, boolean contractAdded) {
        super(sender, previousTask);
        this.contractAdded = contractAdded;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public boolean wasContractAdded() {
        return contractAdded;
    }
    
    @Override
    public String toString() {
        String state = " was not added";
        if (contractAdded) {
            state = " was added succesfully";
        }
        return this.getClass().getSimpleName() + " returning that " + ((NewContractTask)previousTask).getContract() + state;
    }
}
