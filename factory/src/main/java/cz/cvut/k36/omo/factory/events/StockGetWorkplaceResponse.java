/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.production.Workplace;
import java.util.List;

/**
 * Response to a StockGetWorkplaceTask containing a list of received Workplaces.
 */
public class StockGetWorkplaceResponse extends Response {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final List<Workplace> workplaces;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public StockGetWorkplaceResponse(EventChannelAccessible sender, StockGetWorkplaceTask previousTask, List<Workplace> workplaces) {
        super(sender, previousTask);
        this.workplaces = workplaces;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public List<Workplace> getWorkplaces() {
        return workplaces;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " returning Workplaces: " + workplaces + " from " + sender + " to " + previousTask.sender;
    }
}
